FROM python:3.11

RUN addgroup -gid 500 bot && useradd -m -u 500 -g 500 -s /bin/bash bot

ARG ROOT=/home/bot/app
WORKDIR $ROOT

RUN git clone https://gitlab.com/sergevv/ds-cv-boxes .
RUN cp .env_example .env

RUN chown -R bot:bot $ROOT

USER bot

RUN curl -sSL https://install.python-poetry.org | python3 -

ENV PATH="/home/bot/.local/bin:$PATH"

RUN poetry install --no-interaction --no-ansi --only bot

cmd [ "make", "start-bot" ]
