# -*- coding: utf-8 -*-
"""Functions to visualize predictions made by object detection models."""
from collections import Counter
from typing import Dict, List, Optional, Tuple, Union

import cv2
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

from cv_boxes.datastructures.predictions import Predictions

FONT_SCALE = 3
FONT_THICKNESS = 3


def get_text_report(
    predictions: Predictions,
    threshold: int = 0,
) -> Dict[Union[int, str], int]:
    """
    Generate a text report based on the predictions.

    Args:
        predictions (Predictions): Object containing prediction data.
        threshold (int): Minimum confidence threshold. Defaults to 0.

    Returns:
        Dict[Union[int, str], int]:
            A dictionary where keys represent class labels and values represent
            the count of occurrences for each class.
    """
    boxes_count = count_boxes(predictions=predictions, threshold=threshold)
    return {
        predictions.get_class_label(class_id): count
        for class_id, count in boxes_count.items()
    }


def draw_report(
    image: np.ndarray,
    predictions: Predictions,
    threshold: float = 0,
    highlight: Optional[List[bool]] = None,
) -> np.ndarray:
    """
    Draw bounding boxes and labels on the input image based on the predictions.

    Args:
        image (np.ndarray):
            The input image.
        predictions (Predictions):
            Predicted bounding boxes and labels.
        threshold (float):
            Confidence threshold for considering predictions. Defaults to 0.5.
        highlight (Optional[List[bool]], optional):
            A list of boolean values indicating whether to highlight (True) or
            not (False) each prediction. If None, all predictions are
            highlighted. Defaults to None.

    Returns:
        np.ndarray: The input image with bounding boxes and labels drawn.
    """
    image = draw_rotated_boxes(image, predictions, threshold, highlight)

    return draw_text_report(image, predictions, threshold)


def draw_rotated_boxes(
    image: np.ndarray,
    predictions: Predictions,
    threshold: float = 0.5,
    highlight: Optional[List[bool]] = None,
) -> np.ndarray:
    """
    Draw rotated bounding boxes on the given image based on the predictions.

    Note: this function expects and returns image in BGR format.

    Args:
        image (np.ndarray):
            The input image.
        predictions (Predictions):
            Predicted bounding boxes and labels.
        threshold (float):
            Confidence threshold for considering predictions. Defaults to 0.5.
        highlight (Optional[List[bool]], optional):
            A list of boolean values indicating whether to highlight (True) or
            not (False) each prediction. If None, all predictions are
            highlighted. Defaults to None.

    Returns:
        np.ndarray: The image with drawn rotated bounding boxes.
    """
    if highlight is None:
        highlight = [False for _ in predictions]

    for prediction, highlighted in zip(predictions, highlight):
        if prediction.score < threshold:
            continue

        poly = prediction.polygon_mask
        color = predictions.get_class_color_bgr(prediction.class_id)
        if highlighted:
            color = predictions.get_class_color_bgr("highlighted")

        cv2.polylines(image, [poly], isClosed=True, color=color, thickness=2)

    return image


def draw_text_report(
    image: np.ndarray,
    predictions: Predictions,
    threshold: float = 0.5,
    padding: int = 3,
) -> np.ndarray:
    """
    Draw text report on the input image.

    Args:
        image (np.ndarray):
            The input image to draw the report on.
        predictions (Predictions):
            Predictions object containing information about the detected
            objects.
        threshold (float):
            The confidence threshold. Predictions exceeding this threshold will
            be highlighted. Defaults to 0.5.
        padding (int):
            The padding around the highlighted regions. Defaults to 3.

    Returns:
        np.ndarray: The image with the text report drawn on it.
    """
    (_, text_height), _ = cv2.getTextSize(
        text="|",
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=FONT_SCALE,
        thickness=FONT_THICKNESS,
    )
    boxes_count = count_boxes(predictions=predictions, threshold=threshold)
    for i, (class_id, count) in enumerate(boxes_count.items()):
        bg_color = predictions.get_class_color_bgr(class_id)
        fg_color = 255, 255, 255
        color_threshold = 127.5
        if sum(bg_color) / 3 > color_threshold:
            fg_color = 0, 0, 0
        label = predictions.get_class_label(class_id)
        draw_text(
            image=image,
            pos=(200, (text_height + padding) * (i + 1)),
            text=f"{label}: {count}",
            fg_color=fg_color,
            bg_color=bg_color,
        )

    return image


def draw_text(
    image: np.ndarray,
    pos: Tuple[int, int],
    text: str,
    fg_color: Tuple[int, int, int] = (200, 200, 200),
    bg_color: Tuple[int, int, int] = (0, 0, 0),
) -> np.ndarray:
    """
    Draw text onto an image.

    Parameters:
        image (np.ndarray):
            The image onto which text will be drawn.
        pos (Tuple[int, int]):
            The x and y coordinates of the top-left corner of the text.
        text (str):
            The text to be drawn onto the image.
        fg_color (Tuple[int, int, int], optional):
            The foreground color of the text represented as a tuple of
            (R, G, B) values. Defaults to (200, 200, 200).
        bg_color (Tuple[int, int, int], optional):
            The background color of the text represented as a tuple of
            (R, G, B) values. Defaults to (0, 0, 0).

    Returns:
        np.ndarray: The image with the text drawn onto it.
    """
    opacity = 0.5
    left, top = pos
    (width, height), _ = cv2.getTextSize(
        text=text,
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=FONT_SCALE,
        thickness=FONT_THICKNESS,
    )
    bottom = top + height + 3
    right = left + width
    sub_img = image[top:bottom, left:right]

    rect = np.full(sub_img.shape, bg_color, dtype=np.uint8)
    res = cv2.addWeighted(sub_img, opacity, rect, 1 - opacity, 1.0)
    res = cv2.putText(
        img=res,
        text=text,
        org=(0, height),
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=FONT_SCALE,
        color=fg_color,
        thickness=FONT_THICKNESS,
        lineType=cv2.LINE_AA,
    )
    image[top:bottom, left:right] = res

    return image


def count_boxes(
    predictions: Predictions,
    threshold: float = 0.5,
) -> Dict[Union[int, str], int]:
    """
    Count the number of boxesin the predictions.

    Count the number of boxes for every class in the predictions that have
    confidence scores greater than or equal to the specified threshold.

    Parameters:
        predictions (Predictions): The predictions containing bounding boxes
            along with their confidence scores.
        threshold (float): The confidence threshold above which boxes are
            counted. Defaults to 0.5.

    Returns:
        Dict[Union[int, str], int]: A dictionary containing counts of boxes
            whose confidence scores meet the threshold. The keys represent
            unique class labels or 'total' for the total count across all
            classes.
    """
    ret = Counter(
        prediction.class_id
        for prediction in predictions
        if prediction.score >= threshold
    )
    ret["total"] = sum(ret.values())

    return ret


def draw_confusion_matrix(
    true_labels: List[str],
    pred_labels: List[str],
    labels: List[str],
) -> np.ndarray:
    """
    Draws a confusion matrix based on the true and predicted labels.

    Parameters:
        true_labels (List[str]): List of true labels.
        pred_labels (List[str]): List of predicted labels.
        labels (List[str]): List of unique labels.

    Returns:
        np.ndarray: An image with confusion matrix.
    """
    total = min(len(true_labels), len(pred_labels))
    true_labels = true_labels[:total]
    pred_labels = pred_labels[:total]
    cm = confusion_matrix(true_labels, pred_labels, labels=labels)
    cm_display = ConfusionMatrixDisplay(cm, display_labels=labels)
    fig, ax = plt.subplots(figsize=(8, 6))
    cm_display.plot(ax=ax, cmap="Blues")
    plt.xlabel("Predicted Label")
    plt.ylabel("True Label")
    plt.xticks(rotation=-10)

    incorrect_preds = sum(
        true_label != pred_label
        for true_label, pred_label in zip(true_labels, pred_labels)
    )
    incorrect_prc = 100 * incorrect_preds / total
    title = f"{incorrect_preds} incorrect of {total} ({incorrect_prc:.4f}%)"
    plt.title(title)

    fig.canvas.draw()
    width, height = fig.canvas.get_width_height()

    return np.frombuffer(
        fig.canvas.tostring_rgb(),  # type: ignore
        dtype=np.uint8,
    ).reshape(height, width, 3)
