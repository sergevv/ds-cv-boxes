# -*- coding: utf-8 -*-
"""This module provides functions to simulate capturing broken images."""
import os
import random
from glob import glob

import cv2
import numpy as np

IMAGES_PATH = "data/raw/images"


def capture_frame(verbose: int = 0) -> np.ndarray:
    """
    Capture a "broken" image.

    Args:
        verbose (int): Verbosity level. Defaults to 0.

    Returns:
        np.ndarray: A NumPy array representing the captured frame.
    """
    return get_broken_image2()


def get_broken_image() -> np.ndarray:
    """
    Generate a "broken" image with specified dimensions and color channels.

    This function selects a random image file from a directory specified by
    IMAGES_PATH. It then loads the selected image file into memory, extracts
    a random part of the image, decodes it, and returns it as a NumPy array.
    This simulates the effect of a partially loaded image.

    Returns:
        np.ndarray: A NumPy array representing the broken image.
    """
    image_path = pick_random_file(IMAGES_PATH)
    image_buffer = load_file_into_buffer(image_path)
    broken_image = get_part(image_buffer, part="random")
    image_array = np.frombuffer(broken_image, np.uint8)
    return cv2.imdecode(image_array, cv2.IMREAD_COLOR)


def get_broken_image2() -> np.ndarray:
    """
    Generate a "broken" image with specified dimensions and color channels.

    This function loads a random image from the specified IMAGES_PATH and then
    allocates a NumPy array in memory without initializing it. The loaded
    image is discarded by the garbage collector, and the allocated memory
    often intersects with its contents. This may cause some visually
    interesting effects simulating a broken image.

    Returns:
        np.ndarray:
            A NumPy array representing the broken image with dimensions
            (1520, 2688, 3) and data type np.uint8.
    """
    image_path = pick_random_file(IMAGES_PATH)
    image_buffer = load_file_into_buffer(image_path)
    image_array = np.frombuffer(image_buffer, np.uint8)
    image = cv2.imdecode(  # noqa: F841; pylint: disable=W0612
        image_array, cv2.IMREAD_COLOR
    )

    return np.empty((1520, 2688, 3), dtype=np.uint8)


def get_broken_image3() -> np.ndarray:
    """
    Generate a "broken" image with specified dimensions and color channels.

    This function allocates a NumPy array in memory without initializing it.
    As a result, the image will contain random memory contents.

    Returns:
        np.ndarray:
            A NumPy array representing the broken image. The array has
            dimensions (1520, 2688, 3) with dtype=np.uint8, representing an
            image with a width of 2688 pixels, a height of 1520 pixels, and 3
            color channels (Red, Green, Blue) represented by unsigned 8-bit
            integers.
    """
    return np.empty((1520, 2688, 3), dtype=np.uint8)


def get_empty_image() -> np.ndarray:
    """
    Generate an empty image as a NumPy array.

    Returns:
        np.ndarray:
            An empty image represented as a NumPy array with shape
            (1520, 2688, 3) and data type np.uint8. The shape represents the
            dimensions of the image (height, width, and number of color
            channels), and the data type indicates that each pixel value is an
            unsigned 8-bit integer, ranging from 0 to 255.
    """
    return np.zeros((1520, 2688, 3), dtype=np.uint8)


def get_part(buffer: bytes, part) -> bytes:
    """
    Select a portion of the buffer from the beginning.

    Args:
        buffer (bytes): The input buffer.
        part (str, float, int):
            Determines the portion of the buffer to select.
            - If 'random', a random part is chosen.
            - If a float between 0 and 1 (inclusive), selects a part as a
            fraction of the buffer size.
            - If an integer, selects the specified number of bytes from the
            beginning.

    Returns:
        bytes: The selected portion of the buffer.

    Raises:
        ValueError: If part is of an unsupported type or out of range.
    """
    size = len(buffer)
    if part == "random":
        part = random.randint(0, size)  # noqa: S311
    elif isinstance(part, float):
        part = int(part * size)
    elif not isinstance(part, int):
        raise ValueError("Not implemented for type {type(part)}")

    if part < 0 or part > size:
        raise ValueError("Part value out of range")

    return buffer[:part] + bytes(size - part)


def load_file_into_buffer(file_path: str) -> bytes:
    """
    Load the content of a file into a memory buffer.

    Parameters:
        file_path (str): The path to the file to be loaded into memory.

    Returns:
        bytes: Content of the file read as bytes and stored in a memory buffer.
    """
    with open(file_path, "rb") as input_file:
        return input_file.read()


def pick_random_file(directory: str) -> str:
    """
    Pick a random JPEG file from the specified directory.

    Args:
        directory (str):
            The directory path from which to select a random JPEG file.

    Returns:
        str: The path of the randomly chosen JPEG file.

    Raises:
        FileNotFoundError: If the specified directory does not exist or if no
                           JPEG files are found.
    """
    if not os.path.exists(directory):
        raise FileNotFoundError("Directory does not exist.")

    files = glob(os.path.join(directory, "*.jpg"))

    if not files:
        raise FileNotFoundError("No JPEG files found in the directory")

    return random.choice(files)  # noqa: S311
