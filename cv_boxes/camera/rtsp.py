# -*- coding: utf-8 -*-
"""
Module for capturing frames from an IP camera stream and saving them as images.

This module provides functionalities to capture frames from an IP camera stream
using OpenCV, and save them as images.
"""
import os
from datetime import datetime
from typing import Tuple

import click
import cv2
import numpy as np
from dotenv import load_dotenv

from cv_boxes.logging import LoggerSingleton


@click.command()
@click.option("-v", "--verbose", count=True, type=int, help="Verbosity level.")
def capture(verbose: int) -> None:
    """
    Capture a frame from a video stream and save it as JPEG file.

    Function captures an image from camera and saves it as a JPEG file.
    The image file is stored with a timestamp as its name.
    """
    cur_time = datetime.today().strftime("%Y%m%d%H%M%S")
    image_path = f"{cur_time}.jpg"
    image = capture_frame(verbose=verbose)
    cv2.imwrite(image_path, image)


def capture_frame(verbose: int = 0, attempts: int = 3) -> np.ndarray:
    """
    Capture a frame from a camera using OpenCV.

    This function utilizes OpenCV (cv2) to capture a frame from a camera.

    Args:
        verbose (int): Verbosity level for logging. Defaults to 0.
        attempts (int): Number of attempts to capture a frame. Defaults to 3.

    Returns:
        np.ndarray:
            Captured image in BGR format as NumPy array with shape
            (height, width, channels).
    """
    logger = LoggerSingleton.get_logger(verbose=verbose)
    cv2.setLogLevel(verbose)

    load_dotenv()
    ip = os.environ.get("CAMERA_IP")
    port = os.environ.get("CAMERA_PORT")
    login = os.environ.get("CAMERA_LOGIN")
    password = os.environ.get("CAMERA_PASS")
    camera_url = (
        f"rtsp://{login}:{password}@{ip}:{port}/"
        "ASAPI/Streaming/Channels/101/picture?snapShotImageType=JPEG"
    )
    camera = cv2.VideoCapture(camera_url)

    img: np.ndarray = np.zeros((1520, 2688, 3), dtype=np.uint8)
    if camera.isOpened():
        for attempt in range(attempts):
            capture_success, img = camera.read(img)

            if capture_success:
                break

            logger.error(
                "Error while capturing image; attempt %s of %s",
                attempt + 1,
                attempts,
            )
        else:
            logger.error("Failed to capture image after %s attempts", attempts)
    else:
        logger.error("Unable to open camera")

    camera.release()
    cv2.destroyAllWindows()

    return img


def get_image_size(device: cv2.VideoCapture) -> Tuple[int, int]:
    """
    Retrieve width and height of the frames captured by the provided device.

    Parameters:
        device (cv2.VideoCapture):
            A video capture device from which frame size will be obtained.

    Returns:
        Tuple[int, int]:
            A tuple containing the width and height of the frames captured by
            the device.
    """
    width = int(device.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(device.get(cv2.CAP_PROP_FRAME_HEIGHT))

    return width, height
