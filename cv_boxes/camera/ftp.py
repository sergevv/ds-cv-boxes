# -*- coding: utf-8 -*-
"""
Module for simulating camera while interacting with an FTP server.

This module provides functions to capture the latest photo from an FTP server,
retrieve file lists with modification dates, filter JPEG images, download
files, and rename files on the server.
"""
import ftplib
import os
import re
from datetime import datetime, timezone
from io import BytesIO
from typing import List, Optional, Tuple

import cv2
import numpy as np
from dotenv import load_dotenv

from cv_boxes.logging import LoggerSingleton
from cv_boxes.models.constants import DATE_FORMAT

load_dotenv()
FTP_URI = os.environ.get("FTP_URI", "")
FTP_LOGIN = os.environ.get("FTP_LOGIN", "")
FTP_PASS = os.environ.get("FTP_PASS", "")
FTP_TIMOUT = 100
MIN_JPEG_SIZE = 300000
PATTERN = re.compile(r"(--ffff-192.168.0.111_01_)(\d{14})\d{3}_TIMING\.jpg$")
PATTERNS = (
    re.compile(r"(--ffff-192.168.0.111_01_)(\d{14})\d{3}_TIMING\.jpg$"),
    re.compile(r"(--ffff-192.168.0.111_01_)(\d{14})\d{3}_MD_WITH_TARGET\.jpg"),
    re.compile(r"(192.168.0.111_01_)(\d{14})\d{3}_TIMING\.jpg"),
    re.compile(r"(192.168.0.111_01_)(\d{14})\d{3}_MD_WITH_TARGET\.jpg"),
    re.compile(r"(_01_)(\d{14})\d{3}_TIMING\.jpg"),
    re.compile(r"(_01_)(\d{14})\d{3}_MD_WITH_TARGET\.jpg"),
)


def capture_frame(verbose: int = 0, attempts: int = 3) -> np.ndarray:
    """
    Download the latest photo from the FTP server and return it as NumPy array.

    Args:
        verbose (int): Verbosity level for logging. Defaults to 0.
        attempts (int): Number of attempts to capture a frame. Defaults to 3.

    Returns:
        np.ndarray:
            Captured image in BGR format as NumPy array with shape
            (height, width, channels).
    """
    img: np.ndarray = np.zeros((1520, 2688, 3), dtype=np.uint8)
    latest_image_data = get_latest_image_data()
    if latest_image_data is None:
        return img

    image_name, _ = latest_image_data
    image_bytes = download_file("/", image_name, verbose=verbose)
    if image_bytes is None:
        return img

    jpeg_data = np.frombuffer(image_bytes, dtype=np.uint8)

    return cv2.imdecode(jpeg_data, cv2.IMREAD_COLOR)


def get_latest_image_data() -> Optional[Tuple[str, datetime]]:
    """
    Retrieve image name with latest modification date from the FTP server.

    Returns:
        Tuple[str, datetime]:
            A tuple containing the filename and modification date of the file
            with the latest modification date.
    """
    return get_latest(get_image_list())


def get_image_list(
    directory: str = "/",
    verbose: int = 0,
    attempts: int = 3,
) -> List[Tuple[str, datetime]]:
    """
    Retrieve list of images and their timestamps from directory on FTP server.

    Args:
        directory (str):
            The directory on the FTP server to retrieve the list of files from.
            Defaults to "/".
        verbose (int):
            Logging verbosity level. Defaults to 0.
        attempts (int):
            Number of attempts to retrieve the file list. Defaults to 3.

    Returns:
        List[Tuple[str, datetime]]:
            A list of tuples, where each tuple contains the filename and its
            corresponding modification date.
    """
    file_details: List[str] = []
    file_details = get_mlsd(directory, verbose, attempts)
    images = filter_images(file_details)

    return [(name, modify_time) for name, _, modify_time in parse_mlsd(images)]


def get_mlsd(
    directory: str = "/",
    verbose: int = 0,
    attempts: int = 3,
) -> List[str]:
    """
    Retrieve list of directory entries from FTP server using the MLSD command.

    Args:
        directory (str):
            The directory path on the FTP server to list.
            Defaults to the root directory ("/").
        verbose (int):
            Logging verbosity level. Defaults to 0.
        attempts (int):
            Number of attempts to retrieve the list. Defaults to 3.

    Returns:
        List[str]:
            A list of strings containing the details of files and directories
            in the specified FTP directory.
    """
    file_details: List[str] = []
    for attempt in range(attempts):
        try:
            with ftplib.FTP(
                host=FTP_URI,
                user=FTP_LOGIN,
                passwd=FTP_PASS,
                timeout=FTP_TIMOUT,
            ) as ftp:
                ftp.cwd(directory)
                ftp.retrlines("MLSD", file_details.append)

                return file_details

        except ftplib.all_errors as e:
            logger = LoggerSingleton.get_logger(verbose)
            message = (
                "Error %s occured while retrieving files list from FTP. "
                "Attempt %s of %s"
            )
            logger.error(message, e, attempt + 1, attempts)

    return file_details


def parse_mlsd(mlsd_data: List[str]) -> List[Tuple[str, int, datetime]]:
    """
    Parse MLSD data from FTP server to extract filenames & modification dates.

    Args:
        mlsd_data (List[str]):
            A list of strings where each string contains details about a file.

    Returns:
        List[Tuple[str, datetime]]:
            A list of tuples where each tuple contains the filename, its size,
            and modification date.
    """
    files_with_dates = []
    for details in mlsd_data:
        parts = details.split(";")
        filename = parts[-1].strip()
        size = 0
        for part in parts:
            if part.startswith("modify="):
                _, modify_time_str = part.split("=")
                modify_time = datetime.strptime(modify_time_str, DATE_FORMAT)
                modify_time = modify_time.replace(tzinfo=timezone.utc)
            if part.startswith("size="):
                size = int(part.split("=")[1])

        files_with_dates.append((filename, size, modify_time))

    return files_with_dates


def filter_images(mlsd_data: List[str]) -> List[str]:
    """
    Filter a list of metadata strings to include only JPEG images.

    Args:
        mlsd_data (List[str]):
            A list of metadata strings where each string contains details
            separated by semicolons, with the filename as the last part.

    Returns:
        List[str]: A list of metadata strings corresponding to JPEG images.
    """
    ret = []
    for details in mlsd_data:
        parts = details.split(";")
        filename = parts[-1].strip()
        if filename.lower().endswith(".jpg"):
            ret.append(details)

    return ret


def get_latest(
    files: List[Tuple[str, datetime]],
) -> Optional[Tuple[str, datetime]]:
    """
    Retrieve the file with the latest modification date from a list of files.

    Args:
        files (List[Tuple[str, datetime]]):
            A list of tuples, where each tuple contains the filename and its
            corresponding modification date.

    Returns:
        Tuple[str, datetime]:
            A tuple containing the filename and modification date of the file
            with the latest modification date.
    """
    if not files:
        return None

    latest_file = files[0]
    for filename, modify_time in files:
        if modify_time > latest_file[1]:
            latest_file = (filename, modify_time)

    return latest_file


def download_file(
    directory: str,
    filename: str,
    verbose: int = 0,
    attempts: int = 3,
) -> Optional[bytes]:
    """
    Download a file from an FTP server and return its content as bytes data.

    Args:
        directory (str):
            The directory where file is stored on the FTP server.
        filename (str):
            The name of the file to download from the FTP server.
        verbose (int):
            The verbosity level for logging. Defaults to 0.
        attempts (int):
            The number of attempts to retry downloading in case of failure.
            Defaults to 3.

    Returns:
        Optional[bytes]:
            The content of the downloaded file as bytes data, or None if the
            download fails.
    """
    for attempt in range(attempts):
        try:
            with ftplib.FTP(
                host=FTP_URI,
                user=FTP_LOGIN,
                passwd=FTP_PASS,
                timeout=FTP_TIMOUT,
            ) as ftp:
                ftp.cwd(directory)
                memory_buffer = BytesIO()
                ftp.retrbinary(f"RETR {filename}", memory_buffer.write)
                memory_buffer.seek(0)

                return memory_buffer.getvalue()

        except ftplib.all_errors as e:
            logger = LoggerSingleton.get_logger(verbose)
            message = (
                "Error %s occured while downloading file %s from FTP. "
                "Attempt %s of %s"
            )
            logger.error(message, e, filename, attempt + 1, attempts)

    return None


def upload_file(
    directory: str,
    filename: str,
    buffer: BytesIO,
    verbose: int = 0,
    attempts: int = 3,
) -> bool:
    """
    Upload a file to an FTP server.

    Args:
        directory (str):
            The directory on the FTP server to which the file will be uploaded.
        filename (str):
            The name of the file to be uploaded.
        buffer (BytesIO):
            The file data in a BytesIO buffer.
        verbose (int):
            The verbosity level for logging. Default is 0.
        attempts (int):
            The number of attempts to try uploading the file. Default is 3.

    Returns:
        bool: True if the file uploaded successfully, False otherwise.
    """
    for attempt in range(attempts):
        try:
            with ftplib.FTP(
                host=FTP_URI,
                user=FTP_LOGIN,
                passwd=FTP_PASS,
                timeout=FTP_TIMOUT,
            ) as ftp:
                ftp.cwd(directory)
                ftp.storbinary(f"STOR {filename}", buffer)

                return True

        except ftplib.all_errors as e:
            logger = LoggerSingleton.get_logger(verbose)
            message = (
                "Error %s occured while uploading file %s to FTP. "
                "Attempt %s of %s"
            )
            logger.error(message, e, filename, attempt + 1, attempts)

    return False


def delete_redundant_images(directory: str = "/", verbose: int = 0) -> None:
    """
    Delete redundant images from the specified FTP directory.

    This function connects to an FTP server and deletes files in the specified
    directory that end with ".jpg" and are smaller than 300 KB.

    Args:
        directory (str):
            The directory on the FTP server to check for redundant files.
            Defaults to "/".
        verbose (int):
            The verbosity level for logging. Defaults to 0.
    """
    logger = LoggerSingleton.get_logger(verbose)
    image_data = parse_mlsd(filter_images(get_mlsd(directory, verbose)))
    try:
        with ftplib.FTP(
            host=FTP_URI,
            user=FTP_LOGIN,
            passwd=FTP_PASS,
            timeout=FTP_TIMOUT,
        ) as ftp:
            ftp.cwd(directory)
            for filename, size, _ in image_data:
                if size < MIN_JPEG_SIZE:
                    logger.info("Deleting %s of size %s", filename, size)
                    ftp.delete(filename)

    except ftplib.all_errors as e:
        logger.error("Error %s occured while deleting file from FTP.", e)


def rename_images(directory: str = "/", verbose: int = 0) -> None:
    """
    Rename images in specified FTP directory according to predefined patterns.

    This function connects to an FTP server and renames JPEGs in the specified
    directory based on matching patterns defined in the global PATTERNS list.

    Args:
        directory (str):
            Directory on the FTP server to rename files in. Defaults to "/".
        verbose (int):
            The verbosity level for logging. Defaults to 0.
    """
    logger = LoggerSingleton.get_logger(verbose)
    try:
        with ftplib.FTP(
            host=FTP_URI,
            user=FTP_LOGIN,
            passwd=FTP_PASS,
            timeout=FTP_TIMOUT,
        ) as ftp:
            ftp.cwd(directory)
            for name in ftp.nlst():
                new_name = extract_image_name(name)
                if name != new_name:
                    logger.info("Renaming %s -> %s", name, new_name)
                    ftp.rename(name, new_name)

    except ftplib.all_errors as e:
        logger.error("Error %s occured while renaming file on FTP.", e)


def extract_image_name(name: str) -> str:
    """
    Extract the image name based on predefined patterns.

    Args:
        name (str): The original name of the image file.

    Returns:
        str: The new filename in the format "{modification_time}.jpg".
    """
    for pattern in PATTERNS:
        match = pattern.search(name)
        if match:
            modification_time = match.group(2)
            return f"{modification_time}.jpg"

    return name


def check_file_exists(directory: str, filename: str) -> bool:
    """
    Check if a file exists on an FTP server.

    Args:
        directory (str): The directory on the FTP server where file is located.
        filename (str): The name of the file.

    Returns:
        bool: True if the file exists, False otherwise.
    """
    for details in get_mlsd(directory):
        parts = details.split(";")
        name = parts[-1].strip()
        if name == filename:
            return True

    return False


def delete_file(
    directory: str,
    filename: str,
    verbose: int = 0,
    attempts: int = 3,
) -> bool:
    """
    Delete a file from an FTP server.

    Args:
        directory (str):
            The directory on the FTP server where file is located.
        filename (str):
            The name of the file.
        verbose (int):
            Logging verbosity level. Defaults to 0.
        attempts (int):
            Number of attempts to delete the file list. Defaults to 3.

    Returns:
        bool: True if the file was successfully deleted, False otherwise.
    """
    for attempt in range(attempts):
        try:
            with ftplib.FTP(
                host=FTP_URI,
                user=FTP_LOGIN,
                passwd=FTP_PASS,
                timeout=FTP_TIMOUT,
            ) as ftp:
                ftp.cwd(directory)
                ftp.delete(filename)

                return True

        except ftplib.all_errors as e:
            logger = LoggerSingleton.get_logger(verbose)
            message = (
                "Error %s occured while deleting file from FTP. "
                "Attempt %s of %s"
            )
            logger.error(message, e, attempt + 1, attempts)

    return False


def get_modify_time(directory: str, filename: str) -> Optional[datetime]:
    """
    Retrieve last modification time of a specified file in a given directory.

    Args:
        directory (str):
            The directory path where the file is located.
        filename (str):
            The name of the file for which to retrieve the modification time.

    Returns:
        Optional[datetime]:
            The modification time of the file if found, otherwise None.
    """
    for name, _, modify_time in parse_mlsd(get_mlsd(directory)):
        if name == filename:
            return modify_time

    return None
