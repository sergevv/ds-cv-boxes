# -*- coding: utf-8 -*-
"""Functions for capturing images from Hikvision cameras via API."""
import ctypes
import os
from datetime import datetime
from logging import Logger
from typing import Callable, Optional

import click
import cv2
import numpy as np
from dotenv import load_dotenv
from requests.exceptions import RetryError

from cv_boxes.camera import hcnetsdk
from cv_boxes.logging import LoggerSingleton


@click.command()
@click.option("-v", "--verbose", count=True, type=int, help="Verbosity level.")
def capture(verbose: int) -> None:
    """
    Capture image from camera and save it as JPEG file.

    Function captures an image from camera and saves it as a JPEG file.
    The image file is stored with a timestamp as its name.
    """
    load_dotenv()

    camera = HikvisionApiClient()
    if verbose:
        log_path = os.path.dirname(LoggerSingleton.log_path)
        camera.set_logging(log_path=log_path, verbosity=verbose)

    camera.login(
        port=int(os.environ.get("HKVIS_API_PORT", 0)),
        device_address=os.environ.get("CAMERA_IP", ""),
        user_name=os.environ.get("CAMERA_LOGIN", ""),
        password=os.environ.get("CAMERA_PASS", ""),
    )
    cur_time = datetime.today().strftime("%Y%m%d%H%M%S")
    image_path = f"{cur_time}.jpg"
    camera.capture_jpeg(image_path)

    camera.cleanup()


def capture_frame(verbose: int = 0) -> np.ndarray:
    """
    Capture image from a Hikvision camera.

    Args:
        verbose (int): Verbosity level for logging. Defaults to 0.

    Returns:
        np.ndarray: NumPy array containing the captured image.
    """
    load_dotenv()

    camera = HikvisionApiClient()
    if verbose:
        log_path = os.path.dirname(LoggerSingleton.log_path)
        camera.set_logging(log_path=log_path, verbosity=verbose)

    camera.login(
        port=int(os.environ.get("HKVIS_API_PORT", 0)),
        device_address=os.environ.get("CAMERA_IP", ""),
        user_name=os.environ.get("CAMERA_LOGIN", ""),
        password=os.environ.get("CAMERA_PASS", ""),
    )
    image = camera.capture()

    camera.cleanup()

    return image


class HikvisionApiClient:
    """A client for interfacing with Hikvision IP cameras."""

    unauthorized_message = "You need to be logged in."

    def __init__(self, channel: int = 1, buf_size: int = 10000000) -> None:
        """
        Initialize the HikvisionApiClient.

        Args:
            channel (int):
                The channel number to connect to (default is 1).
            buf_size (int):
                The size of the image buffer in bytes (default is 10,000,000).
        """
        self._logger: Optional[Logger] = None
        self.user_id = -1
        self._image_buffer = ctypes.create_string_buffer(buf_size)
        self._jpeg_params = hcnetsdk.NET_DVR_JPEGPARA()
        self._jpeg_params.wPicQuality = 0
        self._jpeg_params.wPicSize = 0xFF
        self.channel = channel
        self.real_handle = -1
        self.callback: Optional[Callable] = None
        self.attempts = 3
        self._init_sdk()

    def set_logging(self, log_path: str, verbosity: int) -> None:
        """
        Set logging configurations.

        Args:
            log_path (str): The path to save log files.
            verbosity (int): Verbosity level of logs.

        Raises:
            RuntimeError: If setting logging fails.
        """
        self._logger = LoggerSingleton.get_logger(verbosity)
        cv2.setLogLevel(verbosity)

        auto_delete = True
        success = hcnetsdk.NET_DVR_SetLogToFile(
            verbosity, log_path.encode(), auto_delete
        )
        if not success:
            raise RuntimeError("Failed to set logging.")

    def login(
        self,
        port: int,
        device_address: str,
        user_name: str,
        password: str,
    ) -> None:
        """
        Login to the Hikvision device.

        Args:
            port (int): The port number of the device.
            device_address (str): The IP address or hostname of the device.
            user_name (str): The username for login.
            password (str): The password for login.

        Raises:
            RuntimeError: If already logged in
            RetryError: If login fails after multiple attempts.
        """
        if self.user_id >= 0:
            raise RuntimeError("Already logged in.")

        for attempt in range(self.attempts):
            login_info = hcnetsdk.NET_DVR_USER_LOGIN_INFO()
            login_info.bUseAsynLogin = False
            login_info.wPort = port
            login_info.sDeviceAddress = device_address.encode()
            login_info.sUserName = user_name.encode()
            login_info.sPassword = password.encode()
            device_info = hcnetsdk.NET_DVR_DEVICEINFO_V40()

            self.user_id = hcnetsdk.NET_DVR_Login_V40(
                ctypes.byref(login_info),
                ctypes.byref(device_info),
            )

            if self.user_id >= 0:
                break

            self.log_error(
                "Login error: %s; attempt %s of %s",
                hcnetsdk.NET_DVR_GetLastError(),
                attempt + 1,
                self.attempts,
            )
        else:
            raise RetryError(
                f"Failed to login after {self.attempts} attempts."
            )

    def capture_jpeg(self, file_path: str) -> None:
        """
        Capture a JPEG image and save it to a file.

        Args:
            file_path (str): The path to save the captured JPEG image.

        Raises:
            RuntimeError: If not logged in or capture fails.
        """
        if self.user_id < 0:
            raise RuntimeError(self.unauthorized_message)

        capture_success = hcnetsdk.NET_DVR_CaptureJPEGPicture(
            self.user_id,
            self.channel,
            ctypes.byref(self._jpeg_params),
            file_path.encode(),
        )
        if not capture_success:
            raise RuntimeError(
                f"CaptureJPEGPicture error: {hcnetsdk.NET_DVR_GetLastError()}"
            )

    def capture(self) -> np.ndarray:
        """
        Capture an image to RAM and return it as a NumPy array.

        Returns:
            np.ndarray: The captured image as a NumPy array.

        Raises:
            RuntimeError: If not logged in.
            RetryError: If capture fails after multiple attempts.
        """
        if self.user_id < 0:
            raise RuntimeError(self.unauthorized_message)

        for attempt in range(self.attempts):
            returned_size = hcnetsdk.DWORD(0)
            capture_success = hcnetsdk.NET_DVR_CaptureJPEGPicture_NEW(
                self.user_id,
                self.channel,
                ctypes.byref(self._jpeg_params),
                self._image_buffer,
                ctypes.sizeof(self._image_buffer),
                ctypes.byref(returned_size),
            )

            if capture_success:
                break

            self.log_error(
                "CaptureJPEGPicture to RAM error: %s; attempt %s of %s",
                hcnetsdk.NET_DVR_GetLastError(),
                attempt + 1,
                self.attempts,
            )
        else:
            raise RetryError(
                "Failed to capture JPEG to RAM after "
                f"{self.attempts} attempts."
            )

        jpeg_data = np.frombuffer(
            self._image_buffer.raw, dtype=np.uint8, count=returned_size.value
        )

        return cv2.imdecode(jpeg_data, cv2.IMREAD_COLOR)

    def play(self, window_handle: int, callback: Callable) -> None:
        """
        Start real-time streaming.

        Args:
            window_handle: Handle to window where streaming will be displayed.
            callback: Callback function to process streaming data.

        Raises:
            RuntimeError: If not logged in or already playing or failed to
                          start streaming.
        """
        if self.user_id < 0:
            raise RuntimeError(self.unauthorized_message)

        if self.real_handle >= 0:
            raise RuntimeError("Already playing.")

        preview_info = hcnetsdk.NET_DVR_PREVIEWINFO()
        preview_info.hPlayWnd = window_handle
        preview_info.lChannel = self.channel
        preview_info.dwStreamType = 0
        preview_info.dwLinkMode = 0
        preview_info.bBlocked = True
        preview_info.dwDisplayBufNum = 15
        preview_info.byProtoType = 0
        preview_info.byPreviewMode = 0

        self.callback = hcnetsdk.REALDATACALLBACK(callback)

        self.real_handle = hcnetsdk.NET_DVR_RealPlay_V40(
            self.user_id, ctypes.byref(preview_info), self.callback, None
        )

        if self.real_handle < 0:
            error_code = hcnetsdk.NET_DVR_GetLastError()
            raise RuntimeError(
                f"NET_DVR_RealPlay_V40 failed, error code: {error_code}"
            )

    def stop_play(self):
        """
        Stop real-time streaming.

        Raises:
            RuntimeError: If not playing or failed to stop streaming.
        """
        if self.real_handle < 0:
            raise RuntimeError("Not playing.")

        if not hcnetsdk.NET_DVR_StopRealPlay(self.real_handle):
            error_code = hcnetsdk.NET_DVR_GetLastError()
            raise RuntimeError(
                f"NET_DVR_StopRealPlay failed, error code: {error_code}"
            )
        self.real_handle = -1
        self.callback = None

    def capture_bmp(self, file_path: str) -> None:
        """Capture a BMP image during streaming and save it to a file.

        Args:
            file_path (str): The path to save the captured BMP image.

        Raises:
            RuntimeError: If not logged in or streaming not started or failed
                          to capture picture.
        """
        if self.user_id < 0:
            raise RuntimeError(self.unauthorized_message)
        if self.real_handle < 0:
            raise RuntimeError("Start streaming first.")

        capture_success = hcnetsdk.NET_DVR_CapturePicture(
            self.real_handle,
            file_path.encode(),
        )
        if not capture_success:
            error_code = hcnetsdk.NET_DVR_GetLastError()
            raise RuntimeError(f"CapturePicture error: {error_code}")

    def cleanup(self) -> None:
        """Clean up resources."""
        self.log_out()
        hcnetsdk.NET_DVR_Cleanup()

    def log_out(self) -> None:
        """Log out from the device."""
        if self.user_id >= 0:
            hcnetsdk.NET_DVR_Logout_V30(self.user_id)
            self.user_id = -1

    def log_error(self, msg, *args, **kwargs) -> None:
        """
        Log an error message.

        Args:
            msg (str): The error message format string.
            args: Positional arguments to be formatted into the error message.
            kwargs: Keyword arguments to be formatted into the error message.
        """
        if self._logger is not None:
            self._logger.error(msg, *args, **kwargs)

    def _init_sdk(self) -> None:
        """
        Initialize SDK.

        Raises:
            RuntimeError: If initialization failed.
        """
        if not hcnetsdk.NET_DVR_Init():
            raise RuntimeError("Initialization failed.")
