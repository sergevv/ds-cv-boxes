# -*- coding: utf-8 -*-
"""
Python connections to Hikvision SDK shared libraries.

This module provides Python bindings to interact with Hikvision SDK shared
libraries for accessing Hikvision devices.

Usage:
    Linux:
    Download EN-HCNetSDKV6.1.9.48_build20230410_linux64.zip from
    https://www.hikvision.com/en/support/tools/hitools/clf4633a00e385d6ea/
    extract and copy `lib` directory to project root.

    Windows:
    Download EN-HCNetSDKV6.1.9.48_build20230410_win64.zip from
    https://www.hikvision.com/en/support/tools/hitools/clc33f9cd4117c753a/
    extract and copy `lib` directory to project root.

Constants:
    SERIALNO_LEN: Length of device serial number
    STREAM_ID_LEN: Length of stream ID
    NET_DVR_DEV_ADDRESS_MAX_LEN: Maximum length of device address
    NET_DVR_LOGIN_USERNAME_MAX_LEN: Maximum length of login username
    NET_DVR_LOGIN_PASSWD_MAX_LEN: Maximum length of login password

Classes:
    NET_DVR_DEVICEINFO_V30: Structure containing device information
    NET_DVR_DEVICEINFO_V40: Structure containing extended device information
    NET_DVR_USER_LOGIN_INFO: Structure containing user login information
    NET_DVR_JPEGPARA: Structure containing JPEG parameters
    NET_DVR_PREVIEWINFO: Structure containing preview information

Functions:
    NET_DVR_Init: Initialize SDK
    NET_DVR_SetLogToFile: Set log to file
    NET_DVR_Login_V40: Login to device
    NET_DVR_GetLastError: Get last error
    NET_DVR_CaptureJPEGPicture: Capture JPEG picture
    NET_DVR_CaptureJPEGPicture_NEW: Capture JPEG picture with buffer
    NET_DVR_RealPlay_V40: Start real-time preview
    NET_DVR_StopRealPlay: Stop real-time preview
    NET_DVR_CapturePicture: Capture picture
    NET_DVR_Logout_V30: Logout from device
    NET_DVR_Cleanup: Clean up SDK resources
"""
import ctypes
import sys

# Define constants
SERIALNO_LEN = 48
STREAM_ID_LEN = 32
NET_DVR_DEV_ADDRESS_MAX_LEN = 129
NET_DVR_LOGIN_USERNAME_MAX_LEN = 64
NET_DVR_LOGIN_PASSWD_MAX_LEN = 64
PREVIEWINFO_RESERVED_LEN = 213
DEVICEINFO_V40_RESERVED_LEN = 237
USER_LOGIN_INFO_RESERVED_LEN = 119

# Define ctypes equivalents for C types
BOOL = ctypes.c_int
DWORD = ctypes.c_uint
WORD = ctypes.c_ushort
LONG = ctypes.c_int
BYTE = ctypes.c_ubyte
LPDWORD = ctypes.POINTER(DWORD)
HWND = ctypes.c_void_p
HC_NET_SDK = None

if sys.platform.startswith("linux"):
    HWND = ctypes.c_uint  # type: ignore
    HC_NET_SDK = ctypes.CDLL("lib/libhcnetsdk.so")
elif sys.platform.startswith("win32") or sys.platform.startswith("cygwin"):
    from ctypes.wintypes import HWND  # noqa: WPS440, WPS433

    HC_NET_SDK = ctypes.WinDLL("lib/HCNetSDK.dll")


class NET_DVR_DEVICEINFO_V30(ctypes.Structure):
    """Structure containing device information."""

    _fields_ = [
        ("sSerialNumber", BYTE * SERIALNO_LEN),
        ("byAlarmInPortNum", BYTE),
        ("byAlarmOutPortNum", BYTE),
        ("byDiskNum", BYTE),
        ("byDVRType", BYTE),
        ("byChanNum", BYTE),
        ("byStartChan", BYTE),
        ("byAudioChanNum", BYTE),
        ("byIPChanNum", BYTE),
        ("byZeroChanNum", BYTE),
        ("byMainProto", BYTE),
        ("bySubProto", BYTE),
        ("bySupport", BYTE),
        ("bySupport1", BYTE),
        ("bySupport2", BYTE),
        ("wDevType", WORD),
        ("bySupport3", BYTE),
        ("byMultiStreamProto", BYTE),
        ("byStartDChan", BYTE),
        ("byStartDTalkChan", BYTE),
        ("byHighDChanNum", BYTE),
        ("bySupport4", BYTE),
        ("byLanguageType", BYTE),
        ("byVoiceInChanNum", BYTE),
        ("byStartVoiceInChanNo", BYTE),
        ("bySupport5", BYTE),
        ("bySupport6", BYTE),
        ("byMirrorChanNum", BYTE),
        ("wStartMirrorChanNo", WORD),
        ("bySupport7", BYTE),
        ("byRes2", BYTE),
    ]


class NET_DVR_DEVICEINFO_V40(ctypes.Structure):
    """Structure containing extended device information."""

    _fields_ = [
        ("struDeviceV30", NET_DVR_DEVICEINFO_V30),
        ("bySupportLock", BYTE),
        ("byRetryLoginTime", BYTE),
        ("byPasswordLevel", BYTE),
        ("byProxyType", BYTE),
        ("dwSurplusLockTime", DWORD),
        ("byCharEncodeType", BYTE),
        ("bySupportDev5", BYTE),
        ("bySupport", BYTE),
        ("byLoginMode", BYTE),
        ("dwOEMCode", DWORD),
        ("iResidualValidity", ctypes.c_int),
        ("byResidualValidity", BYTE),
        ("bySingleStartDTalkChan", BYTE),
        ("bySingleDTalkChanNums", BYTE),
        ("byPassWordResetLevel", BYTE),
        ("bySupportStreamEncrypt", BYTE),
        ("byMarketType", BYTE),
        ("byTLSCap", BYTE),
        ("byRes2", BYTE * DEVICEINFO_V40_RESERVED_LEN),
    ]


fLoginResultCallBack = ctypes.CFUNCTYPE(
    None,
    LONG,
    DWORD,
    ctypes.POINTER(NET_DVR_DEVICEINFO_V30),
    ctypes.c_void_p,
)


class NET_DVR_USER_LOGIN_INFO(ctypes.Structure):
    """Structure containing user login information."""

    _fields_ = [
        ("sDeviceAddress", ctypes.c_char * NET_DVR_DEV_ADDRESS_MAX_LEN),
        ("byUseTransport", BYTE),
        ("wPort", WORD),
        ("sUserName", ctypes.c_char * NET_DVR_LOGIN_USERNAME_MAX_LEN),
        ("sPassword", ctypes.c_char * NET_DVR_LOGIN_PASSWD_MAX_LEN),
        ("cbLoginResult", fLoginResultCallBack),
        ("pUser", ctypes.c_void_p),
        ("bUseAsynLogin", ctypes.c_int),
        ("byProxyType", BYTE),
        ("byUseUTCTime", BYTE),
        ("byLoginMode", BYTE),
        ("byHttps", BYTE),
        ("iProxyID", LONG),
        ("byVerifyMode", BYTE),
        ("byRes3", BYTE * USER_LOGIN_INFO_RESERVED_LEN),
    ]


class NET_DVR_JPEGPARA(ctypes.Structure):
    """
    Structure containing JPEG parameters.

    Note: If encoding resolution is VGA, it supports grabbing
    0=CIF, 1=QCIF, 2=D1 image. But if encoding resolution is
    3=UXGA (1600x1200), 4=SVGA (800x600), 5=HD720p (1280x720), 6=VGA,
    7=XVGA, and 8=HD900p it only support grabbing image with current
    resolution

    0-CIF, 1-QCIF, 2-D1, 3-UXGA(1600x1200), 4-SVGA(800x600),
    5-HD720p(1280x720), 6-VGA, 7-XVGA, 8-HD900p, 9-HD1080, 10-2560*1920,
    11-1600*304, 12-2048*1536, 13-2448*2048, 14-2448*1200, 15-2448*800,
    16-XGA(1024*768), 17-SXGA(1280*1024),18-WD1(960*576/960*480),
    19-1080i, 20-576*576, 21-1536*1536, 22-1920*1920, 23-320*240,
    24-720*720, 25-1024*768, 26-1280*1280, 27-1600*600, 28-2048*768,
    29-160*120, 55-3072*2048, 64-3840*2160, 70-2560*1440, 75-336*256,
    78-384*256, 79-384*216, 80-320*256, 82-320*192, 83-512*384,
    127-480*272, 128-512*272, 161-288*320, 162-144*176, 163-480*640,
    164-240*320, 165-120*160, 166-576*720, 167-720*1280, 168-576*960,
    180-180*240, 181-360*480, 182-540*720, 183-720*960, 184-960*1280,
    185-1080*1440 215-1080*720(occupied untested),
    216-360x640(occupied untested),
    245-576*704[P]/480*704[N](occupied untested), 500-384*288,
    0xff-Auto(Use resolution of current stream)
    """

    _fields_ = [
        ("wPicSize", WORD),
        ("wPicQuality", WORD),  # 0 the best, 1 better, 2 average
    ]


class NET_DVR_PREVIEWINFO(ctypes.Structure):
    """Structure containing preview information."""

    _fields_ = [
        ("lChannel", LONG),  # Channel no.
        ("dwStreamType", DWORD),  # Stream type
        ("dwLinkMode", DWORD),  # Protocol type
        ("hPlayWnd", HWND),  # Play window's handle
        ("bBlocked", DWORD),  # Stream requesting process is blocked or not
        ("bPassbackRecord", DWORD),  # Enable passback record
        ("byPreviewMode", BYTE),  # Preview mode
        ("byStreamID", BYTE * STREAM_ID_LEN),  # Stream ID
        ("byProtoType", BYTE),  # Protocol type
        ("byRes1", BYTE),
        ("byVideoCodingType", BYTE),  # Video coding type
        ("dwDisplayBufNum", DWORD),  # Soft player display buffer size
        ("byNPQMode", BYTE),  # NPQ mode
        ("byRecvMetaData", BYTE),  # Whether to receive metadata
        ("byDataType", BYTE),  # Data type
        ("byRes", BYTE * PREVIEWINFO_RESERVED_LEN),  # Reserved
    ]


# Declare functions
REALDATACALLBACK = ctypes.CFUNCTYPE(
    None, LONG, DWORD, ctypes.POINTER(BYTE), DWORD, ctypes.c_void_p
)

NET_DVR_Init = HC_NET_SDK.NET_DVR_Init
NET_DVR_Init.restype = BOOL

NET_DVR_SetLogToFile = HC_NET_SDK.NET_DVR_SetLogToFile
NET_DVR_SetLogToFile.argtypes = [
    DWORD,
    ctypes.c_char_p,
    BOOL,
]
NET_DVR_SetLogToFile.restype = BOOL

NET_DVR_Login_V40 = HC_NET_SDK.NET_DVR_Login_V40
NET_DVR_Login_V40.argtypes = [
    ctypes.POINTER(NET_DVR_USER_LOGIN_INFO),
    ctypes.POINTER(NET_DVR_DEVICEINFO_V40),
]
NET_DVR_Login_V40.restype = LONG

NET_DVR_GetLastError = HC_NET_SDK.NET_DVR_GetLastError
NET_DVR_GetLastError.restype = DWORD

NET_DVR_CaptureJPEGPicture = HC_NET_SDK.NET_DVR_CaptureJPEGPicture
NET_DVR_CaptureJPEGPicture.argtypes = [
    LONG,
    LONG,
    ctypes.POINTER(NET_DVR_JPEGPARA),
    ctypes.c_char_p,
]
NET_DVR_CaptureJPEGPicture.restype = BOOL

# Grab JPEG and save to RAM
NET_DVR_CaptureJPEGPicture_NEW = HC_NET_SDK.NET_DVR_CaptureJPEGPicture_NEW
NET_DVR_CaptureJPEGPicture_NEW.argtypes = [
    LONG,
    LONG,
    ctypes.POINTER(NET_DVR_JPEGPARA),
    ctypes.c_char_p,
    DWORD,
    LPDWORD,
]
NET_DVR_CaptureJPEGPicture_NEW.restype = BOOL

NET_DVR_RealPlay_V40 = HC_NET_SDK.NET_DVR_RealPlay_V40
NET_DVR_RealPlay_V40.argtypes = [
    LONG,
    ctypes.POINTER(NET_DVR_PREVIEWINFO),
    REALDATACALLBACK,
    ctypes.c_void_p,
]
NET_DVR_RealPlay_V40.restype = LONG

NET_DVR_StopRealPlay = HC_NET_SDK.NET_DVR_StopRealPlay
NET_DVR_StopRealPlay.argtypes = [LONG]
NET_DVR_StopRealPlay.restype = BOOL

NET_DVR_CapturePicture = HC_NET_SDK.NET_DVR_CapturePicture
NET_DVR_CapturePicture.argtypes = [LONG, ctypes.c_char_p]
NET_DVR_CapturePicture.restype = BOOL

NET_DVR_Logout_V30 = HC_NET_SDK.NET_DVR_Logout_V30
NET_DVR_Logout_V30.argtypes = [LONG]
NET_DVR_Logout_V30.restype = BOOL

NET_DVR_Cleanup = HC_NET_SDK.NET_DVR_Cleanup
NET_DVR_Cleanup.restype = BOOL
