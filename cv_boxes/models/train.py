# -*- coding: utf-8 -*-
"""
Train MMDetection model based on the provided configuration file.

Example:
    poetry run train -c checkpoints/mask-rcnn.py -vvv
"""
import os
from logging import Logger
from pathlib import Path

import click
import git
import mlflow
import numpy as np
import torch
from dotenv import load_dotenv
from mmdet.models.detectors.base import BaseDetector
from mmdet.utils import setup_cache_size_limit_of_dynamo
from mmengine.config import Config
from mmengine.runner import Runner
from PIL import Image
from tqdm import tqdm

from cv_boxes.data.validator import remove_invalid_predictions
from cv_boxes.datastructures.dataset import CocoDataset
from cv_boxes.logging import LoggerSingleton
from cv_boxes.models.mmdet import predict
from cv_boxes.visualization.visualize import draw_confusion_matrix, draw_report


@click.command()
@click.option(
    "-c",
    "--config-path",
    default=None,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help="Path to the configuration file for the model.",
)
@click.option(
    "-v",
    "--verbose",
    count=True,
    type=int,
    help=(
        "Verbosity level for logging messages. Increase this value to "
        "increase the amount of logged information."
    ),
)
def train(config_path: Path, verbose: int) -> None:
    """
    Train MMDetection model based on the provided configuration file.

    \b
    Example:
        poetry run train -c checkpoints/mask-rcnn.py -vvv
    """
    logger = LoggerSingleton.get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    load_dotenv()
    setup_cache_size_limit_of_dynamo()

    config = load_config(config_path)
    logger.debug("Config file %s loaded.", config_path)

    runner = Runner.from_cfg(config)
    backend = runner.visualizer.get_backend("MLflowVisBackendCustom")
    run_id = backend.experiment.active_run().info.run_id

    logger.debug("Starting train with run id %s.", run_id)
    runner.train()
    runner.model.cfg = config
    runner.model.dataset_meta = config.metainfo
    logger.debug("Training fifnished.")

    log_results(model=runner.model, run_id=run_id, logger=logger)


def load_config(config_path: Path) -> Config:
    """
    Load configuration from the specified path.

    Args:
        config_path (Path): Path to the configuration file.

    Returns:
        Config: Configuration object loaded from the file.
    """
    config = Config.fromfile(config_path)
    set_tracking_uri(config)
    return config


def set_tracking_uri(config: Config) -> None:
    """
    Set the tracking URI for MLflowVisBackendCustom.

    Args:
        config (Config): Configuration object.
    """
    for vis_backend in config.visualizer.vis_backends:
        if vis_backend.type == "MLflowVisBackendCustom":
            vis_backend.tracking_uri = os.environ.get("MLFLOW_TRACKING_URI")
            break


def log_results(model: BaseDetector, run_id: str, logger: Logger) -> None:
    """
    Log artifacts after model training to MLflow Tracking.

    Runs inference on validation dataset using the provided model, logs
    results, and saves the model to MLflow Tracking.

    Args:
        model (BaseDetector): Instance of a trained model.
        run_id (str): Unique identifier for the MLflow run.
        logger (Logger): Logger instance for logging messages.

    Notes:
        - Requires mlflow, git, PIL, numpy, tqdm, and mmdet to be installed.
        - The model's config must include validation dataset information.
        - The dataset must conform to COCO format.
    """
    with mlflow.start_run(run_id=run_id):
        repo = git.Repo(search_parent_directories=True)
        version = repo.head.object.hexsha
        mlflow.set_tag("mlflow.source.git.commit", version)

        logger.debug("Running inference on val dataset.")
        eval_model(model)

        logger.debug("Saving model to MLflow Tracking.")
        mlflow.pytorch.log_model(model, "mmdetection_model")

        logger.debug("Saving model state dict to MLflow Tracking.")
        log_state_dict(model=model)


def eval_model(model: BaseDetector) -> None:
    """
    Evaluate the performance of the given model on the validation dataset.

    Args:
        model (BaseDetector): The detection model to evaluate.

    Notes:
        This function assumes that the validation dataset is in COCO format and
        uses MLflow to log evaluation results such as incorrect predictions and
        confusion matrix.

    Example:
        To evaluate a model 'detector' on the validation dataset, use:
        >>> eval_model(detector)
    """
    val_dataset = model.cfg.val_dataloader.dataset
    image_dir = val_dataset.data_prefix.img
    data_root = val_dataset.data_root
    ann_file = val_dataset.ann_file
    annotations_path = Path(os.path.join(data_root, ann_file))
    dataset = CocoDataset()
    dataset.load(annotations_path=annotations_path)
    true_labels = []
    pred_labels = []
    for image_data in tqdm(dataset):
        image_path = os.path.join(data_root, image_dir, image_data.name)
        image = np.array(Image.open(image_path).convert("BGR;24"))

        base_name, _ = os.path.splitext(image_data.name)
        predictions = remove_invalid_predictions(
            predictions=predict(model, image),
            date=base_name,
            image_size=(image_data.width, image_data.height),
        )
        incorrect_predictions = []
        for true_id, pred_id in zip(*predictions.eval(image_data)):
            true_label = dataset.get_class_label(true_id)
            true_labels.append(true_label)
            pred_label = predictions.get_class_label(pred_id)
            pred_labels.append(pred_label)
            incorrect_predictions.append(true_label != pred_label)

        # log incorrect predictions
        if any(incorrect_predictions):
            image = draw_report(
                image=image,
                predictions=predictions,
                threshold=0,
                highlight=incorrect_predictions,
            )
            mlflow.log_image(
                image=image[:, :, ::-1],  # BGR to RGB,
                artifact_file=f"incorrect_predictions/{image_data.name}",
            )

    labels = ["Background"] + list(dataset.categories.values())
    cm = draw_confusion_matrix(true_labels, pred_labels, labels)
    mlflow.log_image(cm, "confusion_matrix.png")


def log_state_dict(model: BaseDetector) -> None:
    """
    Log model state dict to MLflow Tracking.

    Saves the state dictionary of the provided model to a temporary directory,
    logs it as an MLflow artifact, and removes the temporary file afterwards.

    Parameters:
        model (BaseDetector): The model whose state dictionary is to be saved.
    """
    temp_dir = model.cfg.temp_dir
    state_dict_path = os.path.join(temp_dir, "state_dict.pth")
    torch.save(model.state_dict(), state_dict_path)
    mlflow.log_artifact(state_dict_path)
    os.remove(state_dict_path)
