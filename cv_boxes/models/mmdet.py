# -*- coding: utf-8 -*-
"""
Module for making predictions using an MMDetection model.

Environment Variables:
    MMDET_CONFIG_PATH: stores the path to the model configuration file.
    MMDET_CHECKPOINT_PATH: stores the path to the model checkpoint file.
"""
import os
from pathlib import Path
from typing import List, Optional, Tuple

import boto3
import click
import mlflow
import numpy as np
from dotenv import load_dotenv
from mmdet.apis import inference_detector, init_detector
from mmdet.models.detectors.base import BaseDetector
from mmdet.utils import setup_cache_size_limit_of_dynamo
from mmengine.config import Config
from mmengine.registry import init_default_scope
from PIL import Image
from tqdm import tqdm

from cv_boxes.data.auxiliary import get_images_list
from cv_boxes.data.paths import WORK_DIR
from cv_boxes.data.validator import remove_invalid_predictions
from cv_boxes.datastructures.dataset import Annotation, CocoDataset, ImageData
from cv_boxes.datastructures.predictions import Predictions
from cv_boxes.logging import LoggerSingleton

BUCKET_NAME = "mlflow"


@click.command()
@click.option(
    "-i",
    "--images-dir",
    default=Path("data/raw/images/"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Directory containing old and new images.",
)
@click.option(
    "-a",
    "--annotations-path",
    default=Path("data/raw/annotations.json"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help="Path to existing annotations in COCO format.",
)
@click.option(
    "-o",
    "--output-path",
    type=click.Path(path_type=Path),
    help="Path to save the annotations JSON file.",
)
@click.option(
    "-v",
    "--verbose",
    count=True,
    type=int,
    help="Verbosity level for logging.",
)
def update_annotations(
    images_dir: Path,
    annotations_path: Path,
    output_path: Path,
    verbose: int,
) -> None:
    """
    Annotate new images based on predictions from a trained model.

    \b
    Example:
        poetry run update_annotations -o new_annotations.json -vvv
    """
    logger = LoggerSingleton.get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    model = load_model()
    logger.debug("Model loaded.")

    dataset = CocoDataset.from_file(annotations_path)

    annotated_images = {image.name for image in dataset}
    all_images = set(get_images_list(images_dir, ["jpg"]))

    missing_images = annotated_images - all_images
    for missing_image in missing_images:
        dataset.delete_image(missing_image)

    logger.debug("Started annotating new images.")
    image_id = dataset.max_image_id + 1
    annotation_id = dataset.max_annotation_id + 1
    unannotated_iamges = all_images - annotated_images
    for image_name in tqdm(unannotated_iamges):
        image_path = os.path.join(images_dir, image_name)
        image = np.array(Image.open(image_path).convert("BGR;24"))

        annotations = []
        height, width = image.shape[:2]
        base_name, _ = os.path.splitext(image_name)
        predictions = remove_invalid_predictions(
            predictions=predict(model, image),
            date=base_name,
            image_size=(width, height),
        )
        for prediction in predictions:
            poly_mask = prediction.polygon_mask.astype(np.int32).flatten()
            annotation = Annotation(
                id=annotation_id,
                label=prediction.class_id + 1,
                mask=poly_mask.tolist(),
            )
            annotations.append(annotation)
            annotation_id += 1

        height, width, _ = image.shape
        image_data = ImageData(
            identifier=image_id,
            name=image_name,
            size=(width, height),
            annotations=annotations,
        )
        dataset.append_image(image_data)
        image_id += 1

    dataset.dump(output_path)
    logger.debug("Annotations saved to %s.", output_path)


def predict(model: BaseDetector, image: np.ndarray) -> Predictions:
    """
    Perform object detection on the input image using the provided model.

    Args:
        model (BaseDetector): The object detection model in MMDetection format.
        image (np.ndarray): The input image as a NumPy array.

    Returns:
        Predictions:
            An object containing the predicted scores, masks, bounding boxes,
            class IDs, class names, and color palette for the detected objects.
    """
    predictions = inference_detector(model, image)

    bboxes = predictions.pred_instances.bboxes.cpu().numpy()
    masks = predictions.pred_instances.masks.cpu().numpy()

    corrected_masks = cut_masks(masks=masks, bboxes=bboxes)
    corrected_bboxes = adjust_bboxes_positions(model, image, bboxes)

    return Predictions(
        scores=predictions.pred_instances.scores.cpu().tolist(),
        masks=[mask.tolist() for mask in corrected_masks],
        bboxes=corrected_bboxes.tolist(),
        class_ids=predictions.pred_instances.labels.cpu().tolist(),
        classes=model.dataset_meta["classes"],
        palette=model.dataset_meta["palette"],
    )


def cut_masks(masks: np.ndarray, bboxes: np.ndarray) -> List[np.ndarray]:
    """
    Cut masks according to bounding boxes.

    Args:
        masks (np.ndarray):
            Array of masks.
        bboxes (np.ndarray):
            Array of bounding boxes in format (left, top, right, bottom).

    Returns:
        List[np.ndarray]:
            List of masks cropped according to corresponding bounding boxes.
    """
    bboxes = bboxes.astype(np.int32)
    return [
        mask[top:bottom, left:right]
        for mask, (left, top, right, bottom) in zip(masks, bboxes)
    ]


def adjust_bboxes_positions(
    model: BaseDetector,
    image: np.ndarray,
    bboxes: np.ndarray,
) -> np.ndarray:
    """
    Adjust bounding box positions if a Crop transform has been applied.

    This function corrects bounding box positions based on the offset and scale
    obtained from the detector model, which is required if a Crop transform has
    been applied to the input image before inference.

    Args:
        model (BaseDetector):
            The base detector model used for inference.
        image (np.ndarray):
            The input image.
        bboxes (np.ndarray):
            An array of bounding boxes in format [x_min, y_min, x_max, y_max].

    Returns:
        np.ndarray: An array of adjusted bounding boxes.
    """
    offset = get_offset(model)
    if offset is None:
        return bboxes

    x_offset, y_offset = offset
    x_scale, y_scale = get_scale(model)
    image_height, image_width = image.shape[:2]
    dx = x_offset * image_width // x_scale
    dy = y_offset * image_height // y_scale

    return translate_bboxes(bboxes, dx, dy)


def translate_bboxes(bboxes: np.ndarray, dx: int, dy: int) -> np.ndarray:
    """
    Translate bounding boxes by specified deltas along x and y axes.

    Args:
        bboxes (np.ndarray):
            Input array of bounding boxes of shape (N, 4), where N is the
            number of bounding boxes and each box is represented
            as (x_min, y_min, x_max, y_max).
        dx (int):
            Delta to translate along the x-axis.
        dy (int):
            Delta to translate along the y-axis.

    Returns:
        np.ndarray:
            Translated bounding boxes with the same shape as input bboxes,
            where each box is adjusted by dx along x-axis and dy along y-axis.

    Example:
        >>> import numpy as np
        >>> bboxes = np.array([[10, 20, 30, 40], [50, 60, 70, 80]])
        >>> translate_bboxes(bboxes, 5, -10)
        array([[15, 10, 35, 30],
               [55, 50, 75, 70]])
    """
    ret = bboxes.copy()
    ret[:, 0] += dx
    ret[:, 1] += dy
    ret[:, 2] += dx
    ret[:, 3] += dy

    return ret


def get_offset(model: BaseDetector) -> Optional[Tuple[int, int]]:
    """
    Retrieve offset applied during cropping in data preprocessing pipeline.

    Args:
        model (BaseDetector):
            Detector model instance whose preprocessing pipeline is to be
            inspected.

    Returns:
        Optional[Tuple[int, int]]:
            A tuple containing the horizontal and vertical offset applied
            during cropping, if cropping is found in the preprocessing
            pipeline. Returns None if no cropping step is found.
    """
    for step in model.cfg.test_dataloader.dataset.pipeline:
        if step.type == "Crop":
            return step.left, step.top

    return None


def get_scale(model: BaseDetector) -> Tuple[int, int]:
    """
    Get scale used for resizing images in data pipeline.

    Parameters:
        model (BaseDetector): An instance of a detector model.

    Returns:
        Tuple[int, int]:
            A tuple representing the width and height of the scale used for
            resizing images. Defaults to (1920, 1080) if no resize step is
            found in the data pipeline.
    """
    scale = (1920, 1080)
    for step in model.cfg.test_dataloader.dataset.pipeline:
        if step.type == "Resize":
            scale = step.scale
            break

    return scale


def load_model() -> BaseDetector:
    """
    Load a pre-trained object detection model.

    This function first loads the environment variables using `load_dotenv()`.
    It then retrieves the paths for the model configuration and checkpoint
    files from the environment variables `MMDET_CONFIG_PATH` and
    `MMDET_CHECKPOINT_PATH` respectively.

    Args:
        None

    Returns:
        BaseDetector:
            A pre-trained object detection model The model is initialized on
            the CUDA device with index 0.

    Raises:
        None

    Example:
        To load a pre-trained object detection model:

        >>> model = load_model()
    """
    load_dotenv()
    config_path = os.environ.get("MMDET_CONFIG_PATH")
    checkpoint_path = os.environ.get("MMDET_CHECKPOINT_PATH")
    model = init_detector(config_path, checkpoint_path, device="cuda:0")
    model.dataset_meta = model.cfg.metainfo

    return model


def download_model(
    config_uri: str,
    model_uri: str,
) -> BaseDetector:
    """
    Download a model from MLflow Server.

    Args:
        config_uri (str): The URI of the configuration file for the model.
        model_uri (str): The URI of the model to be downloaded.

    Returns:
        BaseDetector: The downloaded model loaded as a BaseDetector instance.

    Example:
    >>> model = download_model(
    >>>    "models:/Baseline/Production",
    >>>    "s3://mlflow/0/1f7a8b4f560d495e85a45610d754ed0b/artifacts/mas...py",
    >>>    logger
    >>> )
    """
    load_dotenv()
    setup_cache_size_limit_of_dynamo()

    config = download_config(config_uri)
    init_default_scope(config.get("default_scope", "mmdet"))

    loaded_model = mlflow.pytorch.load_model(model_uri)
    loaded_model.cfg = config
    loaded_model.dataset_meta = config.metainfo

    return loaded_model


def download_config(config_uri: str) -> Config:
    """
    Download configuration file from MLflow Server.

    Args:
        config_uri (str):
            The URI pointing to the configuration file stored  in an S3 bucket
            accessible through the MLflow Server.

    Returns:
        Config:
            Instance of Config class representing the downloaded configuration.

    Example:
    >>> config = download_config(
    >>>    "s3://mlflow/0/1f7a8b4f560d495e85a45610d754ed0b/artifacts/mas...py"
    >>> )
    """
    load_dotenv()
    minio_client = boto3.client(
        service_name="s3",
        endpoint_url=os.environ.get("MLFLOW_S3_ENDPOINT_URL"),
    )
    object_key = config_uri.replace(f"s3://{BUCKET_NAME}", "")

    response = minio_client.get_object(Bucket=BUCKET_NAME, Key=object_key)
    config = Config.fromstring(response["Body"].read().decode("utf-8"), ".py")
    config.work_dir = WORK_DIR
    config.launcher = "none"

    return config
