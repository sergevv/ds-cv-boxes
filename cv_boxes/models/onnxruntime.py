# -*- coding: utf-8 -*-
"""
Module for making predictions using an ONNX model.

Environment Variables:
    ONNX_MODEL_PATH (str): The path to the ONNX model.
"""
import os
from math import ceil
from typing import List

import cv2
import numpy as np
import onnxruntime as ort
from dotenv import load_dotenv
from scipy.ndimage import zoom

from cv_boxes.datastructures.predictions import Predictions
from cv_boxes.models.constants import CLASSES, PALETTE

SCALE = (2688, 1520)
SIZE_DIVISOR = 32
MEAN = np.array([0.34020374, 0.34406083, 0.34554454])
STD = np.array([0.24275125, 0.23882046, 0.23427734])


def predict(model: ort.InferenceSession, image: np.ndarray) -> Predictions:
    """
    Make predictions using model in ONNX format.

    Args:
        model (Detector): The object detection model in ONNX format.
        image (numpy.ndarray): Input image as a NumPy array.

    Returns:
        Predictions:
            Object containing predicted information including scores, masks,
            bounding boxes, class IDs, class names, and color palette.

    Note:
        model_uri environment variable must be set to the path of ONNX model.
    """
    bboxes, class_ids, masks = model.run(None, {"input": preprocess(image)})

    bboxes = bboxes[0, ...]
    scores = bboxes[:, -1]
    bboxes = bboxes[:, :4].astype(int)
    labels = class_ids[0, ...]
    masks = masks[0, ...]
    masks = postprocess(bboxes=bboxes, masks=masks)
    return Predictions(
        scores=scores.tolist(),
        masks=[mask.tolist() for mask in masks],
        bboxes=bboxes.tolist(),
        class_ids=labels.tolist(),
        classes=CLASSES,
        palette=tuple(PALETTE),
    )


def load_model() -> ort.InferenceSession:
    """
    Load a detector model.

    This function loads a Detector model using the specified ONNX model path
    obtained from the environment variable 'ONNX_MODEL_PATH'. The model is
    loaded onto the CPU with device ID 0.

    Returns:
        Detector: An instance of the loaded Detector model.
    """
    load_dotenv()
    model_uri = os.environ.get("ONNX_MODEL_PATH")

    return ort.InferenceSession(model_uri)


def preprocess(image: np.ndarray) -> np.ndarray:
    """
    Preprocess an input image for model input.

    Args:
        image (np.ndarray):
            Input image array in HWC (Height, Width, Channels) format.

    Returns:
        np.ndarray:
            Preprocessed image array in NCHW (Batch, Channels, Height, Width)
            format with float32 type.
    """
    # Resize image to a predefined scale.
    image = cv2.resize(image, SCALE)
    # Normalize image using predefined mean and standard deviation.
    image = (image - MEAN) / STD
    # Pad image to make its dimensions multiples of a predefined size divisor.
    height, width, _ = image.shape
    x_pad = ceil(width / SIZE_DIVISOR) * SIZE_DIVISOR - width
    y_pad = ceil(height / SIZE_DIVISOR) * SIZE_DIVISOR - height
    pad = ((0, y_pad), (0, x_pad), (0, 0))
    image = np.pad(image, pad)
    # Transpose image from HWC format to CHW format.
    image = np.transpose(image, (2, 0, 1))
    # Add a batch dimension to convert the image from CHW to NCHW format.
    image = np.expand_dims(image, axis=0)

    return image.astype(np.float32)


def postprocess(bboxes: np.ndarray, masks: np.ndarray) -> List[np.ndarray]:
    """
    Post-process masks by resizing and binarizing.

    This function takes a list of bounding boxes and corresponding masks,
    resizes each mask to fit within the dimensions of its respective bounding
    box, and then binarizes the mask. The result is a list of processed masks
    ready for further use or visualization.

    Args:
        bboxes (np.ndarray):
            An array of bounding boxes, where each bounding box is defined
            by four coordinates [left, top, right, bottom].
        masks (np.ndarray):
            An array of masks corresponding to the bounding boxes, where each
            mask is a 2D array.

    Returns:
        List[np.ndarray]:
            A list of processed masks, where each mask is resized to fit within
            its respective bounding box and binarized to have values of 0 or
            255.
    """
    threshold = 0.5
    max_pixel_value = 255

    ret = []
    for bbox, mask in zip(bboxes, masks):
        left, top, right, bottom = bbox
        bbox_width = right - left
        bbox_height = bottom - top
        mask = resize_mask(mask, bbox_width, bbox_height)
        mask = (mask > threshold).astype(np.uint8) * max_pixel_value
        ret.append(mask)

    return ret


def resize_mask(mask: np.ndarray, width: int, height: int) -> List[np.ndarray]:
    """
    Resize given binary mask to the specified width and height.

    This function takes an input mask and adjusts its dimensions to the desired
    width and height by calculating the appropriate scale ratios and applying a
    spline-based zoom.

    Args:
        mask (np.ndarray): Binary mask to be resized as 2D NumPy array.
        width (int): Desired width of resized mask.
        height (int): Desired height of resized mask.

    Returns:
        np.ndarray: Resized binary mask as a 2D NumPy array.

    Notes:
    - The resizing is performed using spline interpolation of order 3,
      which offers a good balance between performance and image quality.

    - Ensure that the input mask is a 2D array, as this function is designed to
      handle binary masks.

    - The function uses the `zoom` method from the `scipy.ndimage` module for
       resizing.
    """
    mask_height, mask_width = mask.shape
    scale_ratio = (height / mask_height, width / mask_width)

    return zoom(mask, scale_ratio, order=3)
