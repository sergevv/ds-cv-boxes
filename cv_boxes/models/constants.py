# -*- coding: utf-8 -*-
"""Global constants used in object detection and visualization."""

CLASSES = (
    "Keenetic Air",
    "Keenetic Viva",
    "Keenetic Giga",
    "Keenetic Start",
    "Keenetic Speedster",
)
PALETTE = (
    (123, 252, 102),
    (193, 18, 125),
    (152, 142, 110),
    (5, 16, 115),
    (204, 194, 109),
)
DATE_FORMAT = "%Y%m%d%H%M%S"
