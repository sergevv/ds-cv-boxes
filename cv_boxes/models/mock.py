# -*- coding: utf-8 -*-
"""Generate random predictions for testing purposes."""
import random
from typing import Any

import numpy as np

from cv_boxes.datastructures.predictions import Predictions
from cv_boxes.models.constants import CLASSES, PALETTE


def predict(model: Any, image: np.ndarray) -> Predictions:
    """
    Generate random predictions for testing purposes.

    This function generates random predictions simulating the output of a model
    for a given image. It is primarily intended for testing purposes where
    actual model predictions are not available or necessary.

    Parameters:
        model (Any): Model object.
        image (np.ndarray): Input image for which predictions are generated.

    Returns:
        Predictions: An instance of Predictions containing random values.
    """
    samples = random.randint(0, 10)  # noqa: S311
    height = image.shape[0]

    bboxes = np.random.randint(0, height, size=(samples, 4)).tolist()

    max_int = np.iinfo(np.int32).max
    min_int = np.iinfo(np.int32).min
    class_ids = np.random.randint(
        low=min_int,
        high=max_int,
        size=samples,
        dtype=np.int32,
    ).tolist()

    return Predictions(
        scores=np.random.rand(samples).tolist(),
        masks=[[[False]] for _ in range(samples)],
        bboxes=bboxes,
        class_ids=class_ids,
        classes=CLASSES,
        palette=PALETTE,
    )


def load_model() -> None:
    """
    Do nothing.

    This function is intended solely for testing purposes. It simulates the
    loading process of a machine learning model without actually loading any
    real model.
    """
