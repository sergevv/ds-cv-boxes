# -*- coding: utf-8 -*-
"""
Module for making predictions using an ONNX model.

Environment Variables:
    ONNX_MODEL_PATH (str): The path to the ONNX model.
"""
import os

import numpy as np
from dotenv import load_dotenv
from mmdeploy_runtime import Detector  # pylint: disable = no-name-in-module

from cv_boxes.datastructures.predictions import Predictions
from cv_boxes.models.constants import CLASSES, PALETTE


def predict(model: Detector, image: np.ndarray) -> Predictions:
    """
    Make predictions using model in ONNX format.

    Args:
        model (Detector): The object detection model in ONNX format.
        image (numpy.ndarray): Input image as a NumPy array.

    Returns:
        Predictions:
            Object containing predicted information including scores, masks,
            bounding boxes, class IDs, class names, and color palette.

    Note:
        model_uri environment variable must be set to the path of ONNX model.

    Example:
        >>> prediction = predict(image)
    """
    bboxes, class_ids, masks = model(image)

    return Predictions(
        scores=bboxes[:, -1].tolist(),
        masks=[mask.tolist() for mask in masks],
        bboxes=bboxes[:, :4].astype(int).tolist(),
        class_ids=class_ids.tolist(),
        classes=CLASSES,
        palette=tuple(PALETTE),
    )


def load_model() -> Detector:
    """
    Load a detector model.

    This function loads a Detector model using the specified ONNX model path
    obtained from the environment variable 'ONNX_MODEL_PATH'. The model is
    loaded onto the CPU with device ID 0.

    Returns:
        Detector: An instance of the loaded Detector model.
    """
    load_dotenv()
    model_uri = os.environ.get("ONNX_MODEL_PATH")

    return Detector(model_path=str(model_uri), device_name="cpu", device_id=0)
