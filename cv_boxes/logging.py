# -*- coding: utf-8 -*-
"""Initialize and configure logger based on verbosity level provided."""
import logging
import os

from cv_boxes.data.paths import WORK_DIR


class LoggerSingleton:
    """Singleton class for creating and configuring the logger instance."""

    directory_permissions = 0o755
    log_format = "[%(asctime)s][%(levelname).3s] %(message)s"
    log_path = os.path.join(WORK_DIR, "cv-boxes.log")
    _instance = None

    @classmethod
    def get_logger(cls, verbose: int) -> logging.Logger:
        """
        Initialize and configure logger instance based on verbosity level.

        Args:
            verbose (int):
                Verbosity level of logger. Valid values are:
                - 0: No logging output is produced.
                - 1: Critical messages only.
                - 2: Error messages.
                - 3: Warning messages.
                - 4: Informational messages.
                - 5: Detailed debug messages.

        Returns:
            logging.Logger: A configured logger instance.
        """
        if cls._instance is None:
            cls._instance = logging.getLogger("cv-boxes")
            level = {
                0: logging.CRITICAL + 1,
                1: logging.CRITICAL,
                2: logging.ERROR,
                3: logging.WARNING,
                4: logging.INFO,
                5: logging.DEBUG,
            }.get(verbose, logging.DEBUG)
            cls._instance.setLevel(level)

            if verbose > 0:
                os.makedirs(
                    name=WORK_DIR,
                    mode=cls.directory_permissions,
                    exist_ok=True,
                )

                log_formatter = logging.Formatter(cls.log_format)

                file_handler = logging.FileHandler(cls.log_path)
                file_handler.setLevel(level)
                file_handler.setFormatter(log_formatter)

                stderr_handler = logging.StreamHandler()
                stderr_handler.setLevel(level)
                stderr_handler.setFormatter(log_formatter)

                cls._instance.addHandler(file_handler)
                cls._instance.addHandler(stderr_handler)

        return cls._instance
