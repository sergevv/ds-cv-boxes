# -*- coding: utf-8 -*-
"""
A Python module for a Telegram bot that sends prediction reports to users.

This module includes functionalities to interact with a Telegram bot, send
prediction reports containing text or images to users, and handle user
interactions.
"""
import os
from io import BytesIO

import click
import requests
from dotenv import load_dotenv
from telegram import ReplyKeyboardMarkup, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from cv_boxes.logging import LoggerSingleton

load_dotenv()
API_URI = os.environ.get("API_URI", "")
MAIN_MENU = 0
TIMEOUT = 300  # seconds
NET_ERROR_MESSAGE = "Oops. Something went wrong. Please try again later."

logger = LoggerSingleton.get_logger(5)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """
    Start the conversation and ask the user to select menu option.

    Parameters:
        update (Update): The incoming update.
        context (ContextTypes.DEFAULT_TYPE): The context.

    Returns:
        int: The next state after processing the user's choice.
    """
    logger.info("%s started new conversation", get_user_name(update))
    if update.message is None:
        return ConversationHandler.END

    reply_keyboard = [["Annotated image", "Text report"], ["Force update"]]
    await update.message.reply_text(
        text="Select an option please.\n",
        reply_markup=ReplyKeyboardMarkup(
            keyboard=reply_keyboard,
            one_time_keyboard=False,
            input_field_placeholder="Select an option please",
        ),
    )
    return MAIN_MENU


async def image_report(
    update: Update,
    context: ContextTypes.DEFAULT_TYPE,
) -> int:
    """
    Send a report containing an image to the user.

    Retrieves an image prediction report from a specified URL and sends it to
    the user via Telegram. If the request to the URL is successful, the image
    content is extracted and sent as a photo. If the request fails, an error
    message is logged. After sending the report, a message prompting the user
    to request a new report is sent.

    Args:
        update (Update):
            The update object representing an incoming update.
        context (ContextTypes.DEFAULT_TYPE):
            The context object for the handler to access information, such as
            the bot's token or the user's input data.

    Returns:
        int: The next state after processing the user's choice.

    Raises:
        None
    """
    logger.info("%s requested an image report", get_user_name(update))
    if update.message is None:
        return ConversationHandler.END

    response = requests.get(
        url=f"{API_URI}/predict-image/",
        timeout=TIMEOUT,
    )
    if response.status_code == requests.codes.OK:
        out = BytesIO()
        out.write(response.content)
        out.seek(0)
        await update.message.reply_photo(photo=out)
        logger.info("Sent image report to %s", get_user_name(update))
    else:
        await update.message.reply_text(text=NET_ERROR_MESSAGE)
        message = "GET request failed. Status code: %d"
        logger.error(message, response.status_code)

    return MAIN_MENU


async def text_report(
    update: Update,
    context: ContextTypes.DEFAULT_TYPE,
) -> int:
    """
    Send a text report to the user.

    Fetches a text prediction report from a local server and sends it as a
    message to the user. Uses the Telegram bot API to handle messages.

    Args:
        update (Update): The incoming update from the Telegram bot.
        context (ContextTypes.DEFAULT_TYPE): The context of the Telegram bot.

    Returns:
        int: The next state after processing the user's choice.
    """
    logger.info("%s requested a text report", get_user_name(update))
    if update.message is None:
        return ConversationHandler.END

    response = requests.get(
        url=f"{API_URI}/predict-text/",
        timeout=TIMEOUT,
    )
    if response.status_code == requests.codes.OK:
        report = []
        for label, count in response.json().items():
            report.append(f"{label}: {count}")
        await update.message.reply_text(text="\n".join(report))
        logger.info("Sent text report to %s", get_user_name(update))
    else:
        await update.message.reply_text(text=NET_ERROR_MESSAGE)
        message = "GET request failed. Status code: %d"
        logger.error(message, response.status_code)

    return MAIN_MENU


async def force_update(
    update: Update,
    context: ContextTypes.DEFAULT_TYPE,
) -> int:
    """
    Initiate a report update upon user request.

    This function handles the force update command from the user by sending
    a POST request to a predefined API endpoint. If the update is successfully
    initiated, it notifies the user. If the update fails, it informs the user
    of the network error.

    Args:
        update (Update):
            The update object containing information about the user's message
            and context.
        context (ContextTypes.DEFAULT_TYPE):
            The context in which the handler is running.

    Returns:
        int:
            The next state in the conversation. If no message is present in
            the update, ends the conversation and returns
            ConversationHandler.END.
    """
    logger.info("%s requested a force update", get_user_name(update))
    if update.message is None:
        return ConversationHandler.END

    response = requests.post(url=f"{API_URI}/update", timeout=TIMEOUT)
    if response.status_code == requests.codes.OK:
        await update.message.reply_text(text=response.json())
    else:
        await update.message.reply_text(text=NET_ERROR_MESSAGE)
        message = "POST request failed. Status code: %d"
        logger.error(message, response.status_code)

    return MAIN_MENU


def get_user_name(update: Update) -> str:
    """
    Retrieve the first name of the user from the given Telegram Update object.

    Args:
        update (Update): The Telegram Update object.

    Returns:
        str: first name of the user if available, otherwise an empty string.
    """
    if update.message is None:
        return ""

    user = update.message.from_user
    return f"{user.name} (id {user.id})" if user else ""


async def error_handler(
    update: object, context: ContextTypes.DEFAULT_TYPE
) -> None:
    """
    Handle errors that occur during the bot's operation.

    This function is called whenever an exception is raised within any of the
    handlers. It logs the error. If the update is an instance of
    `telegram.Update` and contains a message, it sends a message to the user
    indicating that an error has occurred and asks them to try again later.

    Args:
        update (object):
            The update that caused the error.
        context (ContextTypes.DEFAULT_TYPE):
            The context in which the error occurred.

    Example:
        To register this error handler in your bot, use the following code:

        application.add_error_handler(error_handler)
    """
    logger.error("Exception while handling an update:", exc_info=context.error)
    if isinstance(update, Update) and update.message:
        await update.message.reply_text(
            text="An error occurred. Please try again later."
        )


@click.command()
def main() -> None:
    """Initialize and run the Telegram bot."""
    logger.info("Bot started.")
    token = os.environ.get("TELEGRAM_TOKEN", "")
    application = Application.builder().token(token).build()

    start_handler = CommandHandler("start", start)
    image_report_handler = MessageHandler(
        filters=filters.Regex("^Annotated image$"),
        callback=image_report,
    )
    text_report_handler = MessageHandler(
        filters=filters.Regex("^Text report$"),
        callback=text_report,
    )
    force_update_handler = MessageHandler(
        filters=filters.Regex("^Force update$"),
        callback=force_update,
    )
    conv_handler = ConversationHandler(
        entry_points=[start_handler],
        states={
            MAIN_MENU: [
                image_report_handler,
                text_report_handler,
            ],
        },
        fallbacks=[start_handler],
    )

    application.add_handler(image_report_handler)
    application.add_handler(text_report_handler)
    application.add_handler(force_update_handler)
    application.add_handler(conv_handler)
    application.add_error_handler(error_handler)

    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
