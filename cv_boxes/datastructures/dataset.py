# -*- coding: utf-8 -*-
"""Classes and functions for working with COCO datasets."""
import json
from collections import namedtuple
from pathlib import Path
from typing import Dict, List, Tuple, Union

from shapely.geometry import Polygon

ImageDataTuple = namedtuple(
    "ImageDataTuple",
    ["id", "name", "width", "height", "annotations"],
)
Annotation = namedtuple("Annotation", ["id", "label", "mask"])


class ImageData(ImageDataTuple):
    """Represents image data along with annotations."""

    def __new__(
        cls,
        identifier: int,
        name: str,
        size: Tuple[int, int],
        annotations: List[Annotation],
    ) -> "ImageData":
        """
        Create a new ImageData instance.

        Args:
            identifier (int): The unique identifier of the image.
            name (str): The name of the image file.
            size (Tuple[int, int]): Image size as a tuple (width, height).
            annotations (List[Annotation]): List of annotations for the image.

        Returns:
            ImageData: An instance of ImageData class.
        """
        width, height = size
        return super().__new__(
            cls,
            id=identifier,
            name=name,
            width=width,
            height=height,
            annotations=annotations,
        )

    def find_ground_truth(
        self,
        mask,
        iou_threshold: float = 0.5,
    ) -> Tuple[int, int]:
        """
        Find the closest mask for a given one among annotations.

        Args:
            mask:
                The mask to compare against annotations.
            iou_threshold (float):
                The threshold for Intersection over Union (IoU) to consider a
                match.

        Returns:
            Tuple[int, int]:
                A tuple containing the label and annotation ID of the closest
                match.
        """
        poly = Polygon(mask)
        max_iou = iou_threshold
        label = -1
        annotation_id = -1

        for annotation in self.annotations:
            xs = annotation.mask[0::2]  # noqa: WPS349
            ys = annotation.mask[1::2]
            anno_poly = Polygon(zip(xs, ys))

            intersection_area = poly.intersection(anno_poly).area
            union_area = poly.union(anno_poly).area
            iou = intersection_area / union_area if union_area > 0 else 0
            if iou > max_iou:
                max_iou = iou
                label = annotation.label
                annotation_id = annotation.id

        return label, annotation_id


class CocoDataset:
    """Represents a COCO dataset containing image annotations."""

    def __init__(self) -> None:
        """
        Initialize an instance of CocoDataset.

        Attributes:
            licenses (None): List of license attributes.
            dataset_info (None): Dataset information attributes.
            categories (None): Mapping of category IDs to names.
            images (None): List of ImageData objects representing images
                           in the dataset.
        """
        self.licenses: List[Dict[str, str]] = []
        self.dataset_info: Dict[str, str] = {}
        self.categories: Dict[int, str] = {}
        self.images: List[ImageData] = []

    def load(self, annotations_path: Union[Path, str]) -> None:
        """
        Load COCO dataset from annotations file.

        Args:
            annotations_path (str): The path to the COCO annotations file.
        """
        with open(annotations_path, encoding="utf-8") as input_file:
            annotations = json.load(input_file)

        self.licenses = annotations["licenses"]
        self.dataset_info = annotations["info"]
        self.categories = {
            cat["id"]: cat["name"] for cat in annotations["categories"]
        }

        images = {
            image["id"]: ImageData(
                identifier=image["id"],
                name=image["file_name"],
                size=(image["width"], image["height"]),
                annotations=[],
            )
            for image in annotations["images"]
        }
        for annotation in annotations["annotations"]:
            image_id = annotation["image_id"]
            images[image_id].annotations.append(
                Annotation(
                    id=annotation["id"],
                    label=annotation["category_id"],
                    mask=annotation["segmentation"][0],
                ),
            )

        self.images = list(images.values())

    def dump(self, annotations_path: Union[Path, str]) -> None:
        """
        Dump COCO dataset to annotations file.

        Args:
            annotations_path (str): Path to save the COCO annotations file.
        """
        categories = []
        for id_, name in self.categories.items():
            categories.append({"id": id_, "name": name, "supercategory": ""})

        images = []
        annotations = []
        for image in self.images:
            images.append(
                {
                    "id": image.id,
                    "width": image.width,
                    "height": image.height,
                    "file_name": image.name,
                    "license": 0,
                    "flickr_url": "",
                    "coco_url": "",
                    "date_captured": 0,
                },
            )
            for annotation in image.annotations:
                xs = annotation.mask[0::2]  # noqa: WPS349
                ys = annotation.mask[1::2]
                poly = Polygon(zip(xs, ys))

                left = min(xs)
                right = max(xs)
                top = min(ys)
                bottom = max(ys)
                width = right - left
                height = bottom - top

                annotations.append(
                    {
                        "id": annotation.id,
                        "image_id": image.id,
                        "category_id": annotation.label,
                        "segmentation": [annotation.mask],
                        "area": poly.area,
                        "bbox": [left, top, width, height],
                        "iscrowd": 0,
                        "attributes": {"occluded": False},
                    },
                )

        dataset_dict = {
            "licenses": self.licenses,
            "info": self.dataset_info,
            "categories": categories,
            "images": images,
            "annotations": annotations,
        }

        with open(annotations_path, "w", encoding="ASCII") as output_file:
            json.dump(dataset_dict, output_file)

    @classmethod
    def from_file(cls, annotations_path: Union[Path, str]) -> "CocoDataset":
        """
        Create a CocoDataset object from a COCO annotations file.

        Args:
            annotations_path (str): The path to the COCO annotations file.

        Returns:
            CocoDataset: A CocoDataset object initialized with the provided
                         annotations file.
        """
        dataset = cls()
        dataset.load(annotations_path)

        return dataset

    @classmethod
    def from_list(
        cls,
        licenses: List[Dict[str, str]],
        dataset_info: Dict[str, str],
        categories: Dict[int, str],
        images: List[ImageData],
    ) -> "CocoDataset":
        """
        Create a CocoDataset object from a list of attributes.

        Args:
            licenses (List[Dict[str, str]]): List of license attributes.
            dataset_info (Dict[str, str]): Dataset information attributes.
            categories (Dict[int, str]): Mapping of category IDs to names.
            images (List[ImageData]): List of ImageData objects.

        Returns:
            CocoDataset:
                Instance of CocoDataset initialized with provided attributes.
        """
        dataset = cls()
        dataset.licenses = licenses
        dataset.dataset_info = dataset_info
        dataset.categories = categories
        dataset.images = images

        return dataset

    def __len__(self):
        """
        Get the total number of images in the dataset.

        Returns:
            int: The number of images in the dataset.
        """
        return len(self.images)

    def __getitem__(self, idx):
        """
        Get an image data instance by index.

        Args:
            idx (int): The index of the image data to retrieve.

        Returns:
            ImageData: The image data instance at the specified index.
        """
        return self.images[idx]

    def __iter__(self):
        """
        Get an iterator over the images in the dataset.

        Yields:
            ImageData: Image data in the dataset.
        """
        yield from self.images

    def get_class_label(self, class_id: Union[int, str]) -> str:
        """
        Get the label associated with a given class ID.

        Args:
            class_id (Union[int, str]): The ID of the class.

        Returns:
            str: The label associated with the class ID.
        """
        if class_id == -1:
            return "Background"

        if isinstance(class_id, str):
            return class_id

        return self.categories.get(int(class_id), str(class_id))

    @property
    def labels(self) -> Tuple[str, ...]:
        """
        Get a tuple of all unique class labels in the dataset.

        Returns:
            Tuple[str, ...]: A tuple containing all unique class labels.
        """
        return tuple(self.categories.values())

    @property
    def max_image_id(self) -> int:
        """
        Get the maximum image ID in the dataset.

        Returns:
            int: The highest image ID in the dataset.
        """
        return max((image.id for image in self), default=0)

    @property
    def max_annotation_id(self) -> int:
        """
        Get the maximum annotation ID in the dataset.

        Returns:
            int: The highest annotation ID in the dataset.
        """
        ret = 0
        for image in self:
            for annotation in image.annotations:
                ret = max(ret, annotation.id)

        return ret

    def delete_image(self, name: str) -> bool:
        """
        Delete an image from the dataset by its name.

        Args:
            name (str): The name of the image to be deleted.

        Returns:
            bool: True if the image was found and deleted, False otherwise.
        """
        for i, image in enumerate(self.images):
            if image.name == name:
                self.images.pop(i)
                return True

        return False

    def append_image(self, image: ImageData) -> None:
        """
        Append an image to the dataset.

        Args:
            image (ImageData): The ImageData object to be added to the dataset.
        """
        self.images.append(image)
