# -*- coding: utf-8 -*-
"""
Utility classes and functions for working with object detection predictions.

Classes:
    Prediction: A class to represent a single prediction made by a model.
    Predictions: A class to represent predictions made by a model.
"""
from collections import namedtuple
from itertools import combinations
from typing import List, Optional, Set, Tuple, Union

import cv2
import numpy as np
from shapely.geometry import Polygon

from cv_boxes.datastructures.dataset import ImageData

BinaryMask = List[List[bool]]  # binary mask of shape (height, width)

PredictionTuple = namedtuple(
    "PredictionTuple",
    ["id", "score", "mask", "bbox", "class_id"],
)


class Prediction(PredictionTuple):
    """A class to represent a single prediction made by a model."""

    def __new__(
        cls,
        identifier: int,
        score: float,
        mask: BinaryMask,
        bbox: List[float],
        class_id: int,
    ) -> "Prediction":
        """
        Create a new Prediction instance.

        Args:
            identifier (int):
                The unique identifier of the prediction.
            score (float):
                The confidence score of the prediction.
            mask (BinaryMask):
                The bitmap mask representing the prediction.
            bbox (List[float]):
                The bounding box coordinates [left, top, right, bottom].
            class_id (int):
                The class ID of the prediction.

        Returns:
            Prediction:
                A Prediction object initialized with the provided parameters.
        """
        return super().__new__(
            cls,
            id=identifier,
            score=score,
            mask=mask,
            bbox=bbox,
            class_id=class_id,
        )

    @property
    def polygon_mask(self) -> np.ndarray:
        """
        Convert bitmap mask to polygon or use bbox if it's impossible.

        Returns:
            np.ndarray:
                The polygon coordinates representing the mask or bbox converted
                to polygon.
        """
        bitmap_mask = np.array(self.mask, dtype=np.uint8)
        poly = self.mask_to_polygon(bitmap_mask)
        if poly is None:
            # use horizontal box if something went wrong
            poly = self.bbox_to_polygon()
        else:
            poly += self.bbox[:2]

        return poly.astype(np.int32)

    def bbox_to_polygon(self) -> np.ndarray:
        """
        Convert bounding box coordinates to polygon.

        Returns:
            np.ndarray: The polygon coordinates representing the bounding box.
        """
        left, top, right, bottom = [int(x) for x in self.bbox]
        poly = [
            [left, top],
            [right, top],
            [right, bottom],
            [left, bottom],
        ]

        return np.array(poly)

    @classmethod
    def mask_to_polygon(cls, mask: np.ndarray) -> Optional[np.ndarray]:
        """
        Convert bitmap mask to polygon.

        Args:
            mask (np.ndarray): The bitmap mask to be converted.

        Returns:
            Optional[np.ndarray]:
                The polygon coordinates representing the mask, or None if
                conversion fails.
        """
        contours, _ = cv2.findContours(
            mask,
            cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE,
        )

        if not contours:
            return None

        polygon = np.squeeze(np.concatenate(contours))
        try:
            rect = cv2.minAreaRect(polygon)
        except cv2.error:
            return None

        return cv2.boxPoints(rect)


class Predictions:
    """A class to represent predictions made by a model."""

    total_color = 255, 255, 255
    invalid_color = 255, 255, 255

    def __init__(
        self,
        scores: List[float],
        masks: List[BinaryMask],
        bboxes: List[List[float]],
        class_ids: List[int],
        classes: Tuple[str, ...],
        palette: Tuple[Tuple[int, int, int], ...],
    ) -> None:
        """
        Initialize Predictions object.

        Args:
            scores (List[float]):
                List of confidence scores. Example: [0.998, 0.763, ...]
            masks (List[BinaryMask]):
                List of binary masks in format (n, height, width).
            bboxes (List[List[float]]):
                List of bounding boxes in format (left, top, right, bottom).
                Example: [[341.9, 298.6, 477.3, 345.3], ...].
            class_ids (List[int]):
                List of class IDs. Example: [3, 3, 1, 0, 0, ...]
            classes (Tuple[str, ...]):
                Tuple of class names. Example: ('Keenetic Start', ...)
            palette (Tuple[Tuple[int, int, int], ...]):
                Tuple of class colors. Example: [(123, 252, 102), ...]
        """
        self.predictions = [
            Prediction(prediction_id, score, mask, bbox, class_id)
            for prediction_id, (score, mask, bbox, class_id) in enumerate(
                zip(scores, masks, bboxes, class_ids),
            )
        ]
        self.classes = classes
        self.palette = palette

    def __len__(self) -> int:
        """
        Get the number of predictions.

        Returns:
            int: Number of predictions.
        """
        return len(self.predictions)

    def __getitem__(self, idx):
        """
        Get prediction by index.

        Args:
            idx: Index of the prediction.

        Returns:
            Prediction: Prediction object.
        """
        return self.predictions[idx]

    def __iter__(self):
        """
        Iterate over predictions.

        Yields:
            Prediction: Prediction object.
        """
        yield from self.predictions

    def get_class_label(self, class_id: Union[int, str]) -> str:
        """
        Get the class label for a given class ID.

        Args:
            class_id (Union[int, str]): Class ID or class name.

        Returns:
            str: Class label.
        """
        if isinstance(class_id, str):
            return class_id

        class_id = int(class_id)
        if 0 <= class_id < len(self.classes):
            return self.classes[class_id]

        return str(class_id)

    def get_class_color_rgb(
        self,
        class_id: Union[int, str],
    ) -> Tuple[int, int, int]:
        """
        Get the RGB color for a given class ID.

        Args:
            class_id (Union[int, str]): Class ID or class name.

        Returns:
            Tuple[int, int, int]: RGB color.
        """
        if class_id == "total":
            return self.total_color

        if isinstance(class_id, str):
            return self.invalid_color

        class_id = int(class_id)
        if 0 <= class_id < len(self.palette):
            return self.palette[class_id]

        return self.invalid_color

    def get_class_color_bgr(
        self,
        class_id: Union[int, str],
    ) -> Tuple[int, int, int]:
        """
        Get the BGR color for a given class ID.

        Args:
            class_id (Union[int, str]): Class ID or class name.

        Returns:
            Tuple[int, int, int]: BGR color.
        """
        return self.get_class_color_rgb(class_id=class_id)[::-1]

    @classmethod
    def from_list(
        cls,
        predictions: List[Prediction],
        classes: Tuple[str, ...],
        palette: Tuple[Tuple[int, int, int], ...],
    ) -> "Predictions":
        """
        Create Predictions object from a list of Prediction objects.

        Args:
            predictions (List[Prediction]): List of Prediction objects.
            classes (Tuple[str, ...]): Tuple of class names.
            palette (Tuple[Tuple[int, int, int], ...]): Tuple of class colors.

        Returns:
            Predictions: Predictions object.
        """
        scores = []
        masks = []
        bboxes = []
        class_ids = []
        for prediction in predictions:
            scores.append(prediction.score)
            masks.append(prediction.mask)
            bboxes.append(prediction.bbox)
            class_ids.append(prediction.class_id)

        return cls(
            scores=scores,
            masks=masks,
            bboxes=bboxes,
            class_ids=class_ids,
            classes=classes,
            palette=palette,
        )

    def nms(self, threshold: float = 0.1) -> "Predictions":
        """
        Perform Non-maximum Suppression (NMS) on predictions.

        Args:
            threshold (float): Threshold for NMS.

        Returns:
            Predictions: Filtered Predictions object.
        """
        dups = set()
        for pred1, pred2 in combinations(self, 2):
            poly1 = Polygon(pred1.polygon_mask)
            poly2 = Polygon(pred2.polygon_mask)

            intersection_area = poly1.intersection(poly2).area
            union_area = poly1.union(poly2).area
            iou = intersection_area / union_area if union_area > 0 else 0
            if iou > threshold:
                dup = pred1.id if pred1.score < pred2.score else pred2.id
                dups.add(dup)

        return self.from_list(
            predictions=[pred for pred in self if pred.id not in dups],
            classes=self.classes,
            palette=self.palette,
        )

    def eval(self, image_data: ImageData) -> Tuple[List[int], List[int]]:
        """
        Evaluate predictions against ground truth.

        Args:
            image_data (ImageData): Ground truth image data.

        Returns:
            Tuple[List[int], List[int]]: True labels and predicted labels.
        """
        true_labels = []
        pred_labels = []
        found_annotations: Set[int] = set()

        # relate predictions to ground truth labels
        for prediction in self:
            poly = prediction.polygon_mask
            true_label, annotation_id = image_data.find_ground_truth(poly)
            found_annotations.add(annotation_id)
            true_labels.append(true_label)
            pred_labels.append(prediction.class_id)

        # add undetected samples as background
        for annotation in image_data.annotations:
            if annotation.id not in found_annotations:
                true_labels.append(annotation.label)
                pred_labels.append("Background")

        return true_labels, pred_labels
