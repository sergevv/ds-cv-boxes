# -*- coding: utf-8 -*-
"""
Module for running a FastAPI server for text and image predictions.

This module contains functions to launch a FastAPI server for predicting text
and bounding boxes on images captured from a camera feed. The server exposes
two endpoints: one for predicting text from a captured image and another for
predicting bounding boxes on an image.

Example:
    To launch the server, execute
    $ poetry run start_api

    Then it can be tested using curl with the following command:
    $ curl -X POST -H "Content-Type: multipart/form-data" \\
        -F "file=@data/raw/images/20230823190346.jpg" \\
        http://localhost:8000/predict-image/ --output out.jpg
"""
import asyncio
import json
import os
import socket
from contextlib import asynccontextmanager
from datetime import datetime, timedelta
from io import BytesIO
from typing import Any, Dict, Union

import click
import cv2
import numpy as np
import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.responses import StreamingResponse
from PIL import Image

from cv_boxes.camera import ftp
from cv_boxes.camera.rtsp import capture_frame
from cv_boxes.data.validator import remove_invalid_predictions
from cv_boxes.logging import LoggerSingleton
from cv_boxes.models.onnxruntime import load_model, predict
from cv_boxes.visualization.visualize import draw_report, get_text_report

load_dotenv()
VERSION = os.environ.get("VERSION", "")
MODEL = load_model()
API_PORT = 8000
VERBOSITY = 5
UPDATE_INTERVAL_SECONDS = int(os.environ.get("UPDATE_INTERVAL_SECONDS", 0))
UPDATE_INTERVAL = timedelta(seconds=UPDATE_INTERVAL_SECONDS)
FORCE_UPDATE_INTERVAL = timedelta(hours=2, minutes=30)  # noqa: WPS432
DATE_FORMAT = "%Y-%m-%d %H:%M:%S %z"
START_YEAR = 1970

logger = LoggerSingleton.get_logger(verbose=VERBOSITY)
message_queue: asyncio.Queue = asyncio.Queue()
reports: Dict[str, Any] = {
    "image": Image.fromarray(np.zeros((1520, 2688, 3), dtype=np.uint8)),
    "text": {"total": 0, "updated": "1970-01-01 00:00:00 +0000"},
    "updated": datetime(year=START_YEAR, month=1, day=1).astimezone(),
}


@asynccontextmanager
async def lifespan(application: FastAPI):
    """
    Manage lifespan of FastAPI application.

    This async context manager function is used to define startup and shutdown
    behavior for the FastAPI application. During startup, it initiates
    background tasks, and during shutdown, it handles the necessary cleanup.

    Args:
        application (FastAPI):
            The FastAPI application instance.

    Yields:
        None:
            Control is yielded back to the FastAPI application after starting
            the background tasks.

    Example:
        app = FastAPI(lifespan=lifespan)
    """
    # start background tasks
    await message_queue.put("ftp")
    asyncio.create_task(continuous_predictions())
    yield
    # stop background tasks
    await message_queue.put("stop")


app = FastAPI(lifespan=lifespan)


@app.get("/version/")
async def version() -> Dict[str, str]:
    """
    Retrieve the current version of the application.

    This endpoint returns a dictionary containing the current version of the
    application. The version is specified by the `VERSION` environment
    variable.

    Returns:
        Dict[str, str]:
            A dictionary with a single key "version" and the corresponding
            version string.
    """
    return {"version": VERSION}


@app.post("/update")
async def update() -> str:
    """
    Trigger report update.

    If the difference between the current datetime and the last update
    timestamp is less than the predefined update interval, it returns a message
    indicating that the data was recently updated and skips the update process.

    If the time difference exceeds the update interval, it puts a message in
    the message queue to initiate an update process and returns a message
    confirming that the update has been started. The function informs that new
    reports will be available after a few minutes.

    Returns:
        str: A message indicating the status of the update process.
    """
    diff = time_since_update()
    if diff < UPDATE_INTERVAL:
        return f"Updated {diff.seconds} seconds ago. Skipping."

    await message_queue.put("update")
    return "Started an update. Please check reports after a couple of minutes."


@app.get("/predict-text/")
async def predict_text() -> Dict[Union[int, str], Union[int, str]]:
    """
    Return a text report.

    Returns:
        Dict[Union[int, str], int]: A textual report of the predictions.
    """
    return reports["text"]


@app.get("/predict-image/")
async def predict_image() -> StreamingResponse:
    """
    Return image report.

    Returns:
        StreamingResponse:
            A streaming response containing the image with bounding boxes.
    """
    return StreamingResponse(
        content=image_to_byte_stream(reports["image"]),
        media_type="image/jpeg",
    )


def image_to_byte_stream(image: Image.Image) -> BytesIO:
    """
    Convert a PIL image to a byte stream in JPEG format.

    Args:
        image (Image.Image): The PIL image.

    Returns:
        BytesIO: A BytesIO object containing the image data in JPEG format.
    """
    ret = BytesIO()
    image.save(ret, format="JPEG")
    ret.seek(0)

    return ret


@click.command()
def start():
    """
    Launch the server.

    Launches the server using uvicorn with the FastAPI application located at
    'cv_boxes.api.main:app'. The server listens on the API_PORT.

    \b
    Example:
        This command is typically executed with `poetry run start_api` at the
        root level of the project. After starting the server, it can be tested
        using curl with the following command:

        \b
        curl -X POST -H "Content-Type: multipart/form-data" \\
            -F "file=@data/raw/images/20230823190346.jpg" \\
            http://localhost:8000/predict-image/ --output out.jpg
    """
    host_ip = socket.gethostbyname(socket.gethostname())
    uvicorn.run("cv_boxes.api.main:app", host=host_ip, port=API_PORT)


async def continuous_predictions():
    """
    Start continuous prediction.

    This function runs an infinite loop that performs the following steps:
    1. Fetch the latest image data from the FTP server.
    2. If the image is new (based on the modification time), download it.
    3. Decode the image and make predictions using the pre-trained model.
    4. Generate a visual report and a textual report from the predictions.
    5. Save visual report as an image file and textual report as a JSON file.
    6. Wait for update interval before repeating the process.

    Server ensures that reports are updated only when new images are available.
    """
    logger.info("Predictions task started in the background.")
    loop = asyncio.get_running_loop()
    running = True
    while running:
        try:
            message = await asyncio.wait_for(
                message_queue.get(), timeout=UPDATE_INTERVAL.seconds
            )
        except asyncio.TimeoutError:
            # No message received, perform other periodic tasks
            if time_since_update() < FORCE_UPDATE_INTERVAL:
                await message_queue.put("ftp")
            else:
                await message_queue.put("update")
        else:
            running = await process_message(loop, message)


async def process_message(
    loop: asyncio.AbstractEventLoop, message: str
) -> bool:
    """
    Process a message and execute corresponding update actions.

    Depending on the content of the `message`, this function triggers different
    update tasks. It supports three types of messages: "ftp", "update", and
    "stop".

    Args:
        loop (asyncio.AbstractEventLoop): The current event loop.
        message (str): The message to be processed.

    Returns:
        bool: Returns False if the message is "stop", signaling to stop the
              loop in `continuous_predictions`. Returns True otherwise, to
              continue processing.

    Actions:
        - "ftp": Triggers an update from the FTP server.
        - "update": Triggers an update from the camera.
        - "stop": Logs the stop action and signals to stop the background task.
    """
    if message == "ftp":
        sync_reports_with_ftp()
        if time_since_update() > UPDATE_INTERVAL:
            await loop.run_in_executor(None, update_from_ftp)
    if message == "update":
        sync_reports_with_ftp()
        if time_since_update() > UPDATE_INTERVAL:
            await loop.run_in_executor(None, update_from_camera)
    if message == "stop":
        logger.info("Stopping predictions task.")
        return False

    return True


def update_from_camera() -> None:
    """
    Capture a frame from the camera and updates reports.

    This function captures a frame from the camera. It then obtains the current
    datetime and converts it to the local timezone. Finally, it updates the
    reports.
    """
    image = capture_frame(verbose=VERBOSITY)
    updated = datetime.now().astimezone()
    update_reports(image=image, modification_time=updated)


def update_from_ftp() -> None:
    """Retrieve latest image data from FTP server and update reports."""
    latest_image_data = ftp.get_latest_image_data()
    if latest_image_data is None:
        return

    image_name, modification_time = latest_image_data
    if modification_time <= reports["updated"]:
        return
    if datetime.now().astimezone() - modification_time < UPDATE_INTERVAL:
        return

    image_bytes = ftp.download_file("/", image_name, verbose=VERBOSITY)
    if image_bytes is None:
        return

    jpeg_data = np.frombuffer(image_bytes, dtype=np.uint8)
    image = cv2.imdecode(jpeg_data, cv2.IMREAD_COLOR)
    update_reports(image=image, modification_time=modification_time)


def update_reports(image: np.ndarray, modification_time: datetime) -> None:
    """Update reports dict with new timestamp, image, and text report."""
    if check_update_flag():
        update_started = ftp.get_modify_time("/", "updating")
        if update_started is not None:
            if datetime.now().astimezone() - update_started <= UPDATE_INTERVAL:
                return

    set_update_flag()
    height, width = image.shape[:2]
    predictions = remove_invalid_predictions(
        predictions=predict(model=MODEL, image=image),
        date=None,
        image_size=(width, height),
    )
    bgr_image = draw_report(image, predictions)
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB).astype("uint8")
    reports["image"] = Image.fromarray(rgb_image)
    reports["text"] = get_text_report(predictions)
    reports["text"]["updated"] = modification_time.strftime(DATE_FORMAT)
    reports["updated"] = modification_time
    sync_reports_with_ftp()
    reset_update_flag()
    logger.info("Reports updated.")


def sync_reports_with_ftp() -> None:
    """
    Synchronize the local reports with those on an FTP server.

    This function downloads the JSON report from the FTP server and checks the
    update timestamp. If the local report is more recent, it uploads the local
    reports to the  FTP server. Otherwise, it downloads the image report and
    updates the local reports with the downloaded data.
    """
    json_bytes = ftp.download_file("/", "report.json", verbose=VERBOSITY)
    if json_bytes is None:
        upload_reports_to_ftp()
        return
    text_report = json.loads(json_bytes)
    updated = datetime.strptime(text_report.get("updated"), DATE_FORMAT)

    if reports["updated"] > updated:
        upload_reports_to_ftp()
        return

    image_bytes = ftp.download_file("/", "report.jpeg", verbose=VERBOSITY)
    if image_bytes is None:
        return
    image_stream = BytesIO(image_bytes)
    image_report = Image.open(image_stream)

    reports["text"] = text_report
    reports["image"] = image_report
    reports["updated"] = updated


def upload_reports_to_ftp() -> None:
    """Upload the current reports to an FTP server."""
    buffer = BytesIO()
    reports["image"].save(buffer, format="JPEG")
    buffer.seek(0)
    ftp.upload_file("/", "report.jpeg", buffer)

    json_data = json.dumps(reports["text"])
    buffer = BytesIO(json_data.encode("utf-8"))
    buffer.seek(0)
    ftp.upload_file("/", "report.json", buffer)


def check_update_flag() -> bool:
    """
    Check if the update flag file exists on the FTP server.

    This function checks if a file named "updating" exists in the root
    directory of the FTP server. The presence of this file indicates that an
    update process is currently ongoing.

    Returns:
        bool: True if the "updating" file exists, False otherwise.
    """
    return ftp.check_file_exists("/", "updating")


def set_update_flag() -> bool:
    """
    Set the update flag file on the FTP server.

    This function uploads an empty file named "updating" to the root directory
    of the FTP server. This file is used as a flag to indicate that an update
    process is in progress.

    Returns:
        bool: True if the file was successfully uploaded, False otherwise.
    """
    return ftp.upload_file("/", "updating", BytesIO())


def reset_update_flag() -> bool:
    """
    Reset the update flag file on the FTP server.

    This function deletes the "updating" file from the root directory of the
    FTP server. This indicates that the update process has completed.

    Returns:
        bool: True if the file was successfully deleted, False otherwise.
    """
    return ftp.delete_file("/", "updating")


def time_since_update() -> timedelta:
    """
    Calculate the time passed since the last update of the report.

    Returns:
        timedelta:
            The time difference between the current time and the report's
            'updated' timestamp.
    """
    return datetime.now().astimezone() - reports["updated"]
