# -*- coding: utf-8 -*-
"""Auxiliary functions for data processing."""
import glob
import json
import os
from math import ceil
from pathlib import Path
from typing import Dict, List, Literal, Tuple

import cv2
import numpy as np
from scipy.ndimage import rotate

MASK_PAD = 20
RIGHT_ANGLE = 90


def load_annotations(path: Path) -> Dict:
    """
    Load annotations from a JSON file.

    Args:
        path (Path): The path to the JSON file containing annotations.

    Returns:
        Dict: A dictionary containing the loaded annotations.
    """
    with open(path, encoding="utf-8") as annotations_file:
        return json.load(annotations_file)


def get_images_list(
    data_dir: Path,
    image_types: List[Literal["bmp", "jpg", "jpeg", "png"]],
) -> List[str]:
    """
    Return a list of image filenames found in the specified data directory.

    Args:
        data_dir (Path):
            The path to the directory containing the images.
        image_types (List[Literal["bmp", "jpg", "jpeg", "png"]]):
            A list of image file extensions to search for. Only images with the
            specified extensions will be included in the returned list.

    Returns:
        List[str]:
            A list of filenames of the images found in the data directory.
    """
    ret: List[str] = []
    for image_type in image_types:
        images = glob.glob(os.path.join(data_dir, f"*.{image_type}"))
        ret.extend(map(os.path.basename, images))

    return ret


def poly2bbox(polygon: np.ndarray, pad: int) -> Tuple[int, int, int, int]:
    """
    Convert a polygon defined by an array of vertices into a bounding box.

    Args:
        polygon (np.ndarray):
            An array of shape (N, 2) representing the vertices of the polygon.
            Each row contains the coordinates (x, y) of a vertex.
        pad (int):
            The padding value to add around the bounding box.

    Returns:
        Tuple[int, int, int, int]:
            A tuple representing the bounding box coordinates
            (left, top, right, bottom), where left and top are the coordinates
            of the top-left corner of the bounding box, and right and bottom
            are the coordinates of the bottom-right corner of the bounding box.
    """
    left = int(np.min(polygon[:, 0])) - pad
    top = int(np.min(polygon[:, 1])) - pad
    right = ceil(np.max(polygon[:, 0])) + pad
    bottom = ceil(np.max(polygon[:, 1])) + pad

    return left, top, right, bottom


def copy_mask_from_image(mask: np.ndarray, image: np.ndarray) -> np.ndarray:
    """
    Copy the region defined by the mask from the image.

    Args:
        mask (numpy.ndarray):
            The array of vertices defining the region to be copied.
        image (numpy.ndarray):
            The image from which the mask area is to be copied.

    Returns:
        numpy.ndarray:
            A numpy array representing the copied area from the image.
    """
    left, top, right, bottom = poly2bbox(polygon=mask, pad=MASK_PAD)
    bounding_box = image[top:bottom, left:right].copy()

    _, (box_width, box_height), box_angle = cv2.minAreaRect(mask)
    if box_width < box_height:
        box_width, box_height = box_height, box_width
        box_angle += RIGHT_ANGLE
    horizontal_box = rotate(bounding_box, box_angle)

    image_height, image_width = horizontal_box.shape[:2]
    center_x = image_width / 2
    center_y = image_height / 2
    left = int(center_x - box_width / 2)
    top = int(center_y - box_height / 2)
    right = int(left + box_width)
    bottom = int(top + box_height)

    return horizontal_box[top:bottom, left:right]
