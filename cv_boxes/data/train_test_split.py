# -*- coding: utf-8 -*-
"""
Module to split a dataset in MS COCO format into random train and test subsets.

This module provides functionality to split a dataset in MS COCO format into
randomly selected train and test subsets. It reads annotations from the input
directory, shuffles the list of images, and then divides them into train and
test subsets based on the specified train size proportion.
"""
import os
import random
import shutil
from pathlib import Path
from typing import List, Tuple

import click
from tqdm import tqdm

from cv_boxes.data.paths import (
    ANNOTATIONS_NAME,
    DETECTION_ANNOTATIONS,
    DETECTION_TEST_DIR,
    DETECTION_TRAIN_DIR,
)
from cv_boxes.datastructures.dataset import CocoDataset, ImageData
from cv_boxes.logging import LoggerSingleton

TRAIN_SIZE = 0.8
IMAGES_DIR = "images"
VERBOSITY = 5


@click.command()
@click.option(
    "--train-dir",
    default=DETECTION_TRAIN_DIR,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Path to the directory containing the train images.",
)
@click.option(
    "--test-dir",
    default=DETECTION_TEST_DIR,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Path to the directory containing the test images.",
)
@click.option(
    "-a",
    "--annotations-path",
    default=DETECTION_ANNOTATIONS,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Path to the annotations in MS COCO format.",
)
def train_test_split(
    train_dir: Path, test_dir: Path, annotations_path: Path
) -> None:
    """
    Split annotations according to train and test directories content.

    \b
    Example:
        poetry run train_test_split
    """
    logger = LoggerSingleton.get_logger(VERBOSITY)
    logger.info("Started train test split.")

    logger.info("Reading annotations file.")
    dataset = CocoDataset.from_file(annotations_path)

    logger.info("Copying train annotations.")
    train_images = set(os.listdir(train_dir))
    train_dataset = CocoDataset.from_list(
        licenses=dataset.licenses,
        dataset_info=dataset.dataset_info,
        categories=dataset.categories,
        images=[image for image in dataset if image.name in train_images],
    )
    train_parent = os.path.dirname(train_dir)
    train_name = os.path.basename(train_dir)
    train_annotations_path = os.path.join(train_parent, f"{train_name}.json")
    train_dataset.dump(train_annotations_path)

    logger.info("Copying test annotations.")
    test_images = set(os.listdir(test_dir))
    test_dataset = CocoDataset.from_list(
        licenses=dataset.licenses,
        dataset_info=dataset.dataset_info,
        categories=dataset.categories,
        images=[image for image in dataset if image.name in test_images],
    )
    test_parent = os.path.dirname(test_dir)
    test_name = os.path.basename(test_dir)
    test_annotations_path = os.path.join(test_parent, f"{test_name}.json")
    test_dataset.dump(test_annotations_path)


@click.command()
@click.option(
    "-i",
    "--input-filepath",
    default=Path("data/raw/"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Path to the directory containing the dataset.",
)
@click.option(
    "-o",
    "--output-filepath",
    default=Path("data/processed/"),
    show_default=True,
    type=click.Path(),
    help="Path to the directory where train and test subsets will be saved.",
)
@click.option(
    "--train-size",
    default=TRAIN_SIZE,
    show_default=True,
    type=click.FloatRange(0, 1),
    help=(
        "Proportion of the dataset to include in the training subset. "
        "Should be a float value between 0 and 1."
    ),
)
@click.option(
    "--random-state",
    default=0,
    show_default=True,
    type=click.IntRange(0),
    help="Seed for the random number generator for reproducibility.",
)
def random_train_test_split(
    input_filepath: Path,
    output_filepath: Path,
    train_size: float,
    random_state: int,
) -> None:
    """
    Split dataset in MS COCO format into random train and test subsets.

    \b
    Example:
        poetry run random_train_test_split
    """
    logger = LoggerSingleton.get_logger(VERBOSITY)
    logger.info("Started random train test split.")

    logger.info("Deleting output dir if it exists.")
    _clean_dir(output_filepath)

    logger.info("Reading annotations file.")
    annotations_path = os.path.join(input_filepath, ANNOTATIONS_NAME)
    dataset = CocoDataset.from_file(annotations_path)

    images_list = _shuffle_images(list(dataset), random_state)
    train_dataset, test_dataset = _split(dataset, images_list, train_size)

    logger.info("Copying train images to new destination.")
    images_path = Path(os.path.join(input_filepath, IMAGES_DIR))
    train_path = Path(os.path.join(output_filepath, "train"))
    _copy_data(images_path, train_path, train_dataset)

    logger.info("Copying test images to new destination.")
    test_path = Path(os.path.join(output_filepath, "val"))
    _copy_data(images_path, test_path, test_dataset)


def _split(
    dataset: CocoDataset,
    images_list: List[ImageData],
    train_size: float,
) -> Tuple[CocoDataset, CocoDataset]:
    """
    Split dataset into train and test subsets based on specified train size.

    Args:
        dataset (CocoDataset):
            The original dataset to split.
        images_list (list):
            List of image data.
        train_size (float):
            The proportion of the dataset to include in the training split,
            Should be a float value between 0 and 1.

    Returns:
        tuple: A tuple containing the training dataset (CocoDataset) and
               testing dataset (CocoDataset).
    """
    train_size = int(train_size * len(images_list))
    train_dataset = CocoDataset.from_list(
        licenses=dataset.licenses,
        dataset_info=dataset.dataset_info,
        categories=dataset.categories,
        images=images_list[:train_size],
    )
    test_dataset = CocoDataset.from_list(
        licenses=dataset.licenses,
        dataset_info=dataset.dataset_info,
        categories=dataset.categories,
        images=images_list[train_size:],
    )

    return train_dataset, test_dataset


def _shuffle_images(images_data: List[ImageData], random_state=0):
    """
    Shuffle the order of images.

    Args:
        images_data (List[ImageData]): List containing images to shuffle.
        random_state (int): Seed value for random shuffling. Defaults to 0.

    Returns:
        list: A shuffled list of image data.
    """
    random.seed(random_state)
    random.shuffle(images_data)

    return images_data


def _clean_dir(path: Path):
    """
    Remove all contents in a given directory.

    Args:
        path (Path): The path of the directory to remove and recreate.
    """
    if os.path.exists(path):
        shutil.rmtree(path)
        os.makedirs(path, exist_ok=True)


def _copy_data(
    source_dir: Path, destination_dir: Path, dataset: CocoDataset
) -> None:
    """
    Copy images and corresponding annotations to the respective directories.

    Args:
        source_dir (Path):
            Path to the directory containing the source images.
        destination_dir (Path):
            Path to the destination directory where images and annotations will
            be copied.
        dataset (CocoDataset):
            Dataset object containing annotations.
    """
    os.makedirs(destination_dir, exist_ok=True)
    dir_name = os.path.basename(destination_dir)
    destination_parent = os.path.dirname(destination_dir)
    filename = f"{dir_name}.json"
    annotations_path = os.path.join(destination_parent, filename)
    dataset.dump(annotations_path)

    for image in tqdm(dataset):
        shutil.copy(
            src=os.path.join(source_dir, image.name),
            dst=os.path.join(destination_dir, image.name),
        )
