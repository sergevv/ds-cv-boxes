# -*- coding: utf-8 -*-
"""Module for converting annotations from VIA COCO to CVAT COCO format."""
import json
from pathlib import Path

import click

from cv_boxes.data.paths import DETECTION_ANNOTATIONS


@click.command()
@click.option(
    "-i",
    "--in-path",
    default=DETECTION_ANNOTATIONS,
    show_default=True,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help="Path to the input VIA COCO annotations file.",
)
@click.option(
    "-o",
    "--out-path",
    type=click.Path(),
    help="Path to save the converted CVAT COCO compatible annotations.",
)
def via_to_cvat(
    in_path: Path,
    out_path: Path,
) -> None:
    """
    Convert annotations from VIA COCO format to CVAT COCO.

    \b
    Example:
        poetry run via_to_cvat -o data/raw/out.json
    """
    with open(in_path, encoding="utf-8") as input_file:
        annotations = json.load(input_file)

    image_ids = {}
    for i, image in enumerate(annotations["images"], start=1):
        image_id = image["id"]
        image_ids[image_id] = i
        image["id"] = i

    for annotation in annotations["annotations"]:
        annotation["image_id"] = image_ids[annotation["image_id"]]
        annotation["category_id"] += 1

    for category in annotations["categories"]:
        category["id"] += 1

    with open(out_path, "w", encoding="utf-8") as output_file:
        json.dump(annotations, output_file)
