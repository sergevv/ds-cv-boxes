# -*- coding: utf-8 -*-
"""This module defines bounding constants."""
from collections import namedtuple

BoundingBox = namedtuple("BoundingBox", ["x_min", "y_min", "x_max", "y_max"])

router_bounds = {
    "19700101000000": BoundingBox(x_min=980, x_max=1800, y_min=550, y_max=830),
    "20230823190345": BoundingBox(x_min=524, x_max=1266, y_min=410, y_max=550),
    "20231120222359": BoundingBox(x_min=980, x_max=1800, y_min=550, y_max=830),
    "20231121220022": BoundingBox(x_min=950, x_max=1750, y_min=550, y_max=800),
    "20231124114021": BoundingBox(x_min=990, x_max=1860, y_min=350, y_max=620),
    "20990101000000": BoundingBox(x_min=0, x_max=0, y_min=0, y_max=0),
}
