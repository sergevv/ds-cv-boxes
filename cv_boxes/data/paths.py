# -*- coding: utf-8 -*-
"""This module defines path constants."""
import os
from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent.parent
DETECTION_DATASET = os.path.join(ROOT_DIR, "data", "raw")
ANNOTATIONS_NAME = "annotations.json"
DETECTION_ANNOTATIONS = os.path.join(DETECTION_DATASET, ANNOTATIONS_NAME)
DETECTION_IMAGES = os.path.join(DETECTION_DATASET, "images")
DETECTION_TRAIN_DIR = os.path.join(ROOT_DIR, "data", "processed", "train")
DETECTION_TEST_DIR = os.path.join(ROOT_DIR, "data", "processed", "val")
WORK_DIR = os.path.join(ROOT_DIR, "work_dirs")
TEMP_DIR = os.path.join(ROOT_DIR, "temp_dir")
