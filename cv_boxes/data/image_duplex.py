# -*- coding: utf-8 -*-
"""This module provides scripts for managing duplicate images."""
import csv
import os
import shutil
from collections import defaultdict
from itertools import combinations
from math import inf
from pathlib import Path
from typing import Dict, Generator, Iterable, Set, Tuple

import click
import imagehash
from PIL import Image
from tqdm import tqdm

HASH_SIZE = 32
THRESHOLD = 16

IMAGE_WIDTH = 1920
IMAGE_HEIGHT = 1920
CROP_LEFT = 170
CROP_TOP = 100
CROP_RIGHT = 500
CROP_BOTTOM = 120
CROP_BOX = (
    CROP_LEFT,
    CROP_TOP,
    IMAGE_WIDTH - CROP_RIGHT,
    IMAGE_HEIGHT - CROP_BOTTOM,
)


@click.command()
@click.argument("input_dir", type=click.Path(exists=True, dir_okay=True))
@click.argument("output_dir", type=click.Path(dir_okay=True))
@click.option(
    "-h",
    "--hashes-path",
    type=click.Path(path_type=Path, dir_okay=False),
    help="The path to the CSV file containing p-hashes of images.",
)
@click.option(
    "--threshold",
    default=THRESHOLD,
    show_default=True,
    type=click.IntRange(1, inf),
    help=(
        "The threshold for considering images as duplicates. Images with a "
        "Hamming distance less than this threshold will be considered "
        "duplicates."
    ),
)
def copy_nodups(
    input_dir: Path,
    output_dir: Path,
    hashes_path: Path,
    threshold: int,
) -> None:
    """
    Filter out duplicates and copy only original images to output_dir.

    Note: you can use threshold up to 32 if non Keenetic are not important.

    \b
    Args:
        input_dir (Path):
            Directory containing the input images.
        output_dir (Path):
            Directory where original images will be copied.

    \b
    Example:
        poetry run copy_nodups -h data/new/hash.csv data/new/images/ out
    """
    # remove previous results from output_dir
    shutil.rmtree(output_dir, ignore_errors=True)
    os.makedirs(output_dir)

    dups = _group_duplicates(hashes_path, threshold)
    for name in dups:
        src_path = os.path.join(input_dir, name)
        trg_path = os.path.join(output_dir, name)
        shutil.copy(src_path, trg_path)


@click.command()
@click.argument("input_dir", type=click.Path(exists=True, dir_okay=True))
@click.argument("output_dir", type=click.Path(dir_okay=True))
@click.option(
    "-h",
    "--hashes-path",
    type=click.Path(path_type=Path, dir_okay=False),
    help="Path to the CSV file containing perceptual hashes of images.",
)
@click.option(
    "--threshold",
    default=THRESHOLD,
    show_default=True,
    type=click.IntRange(1, inf),
    help=(
        "Threshold for considering images as duplicates.Images with a "
        "Hamming distance less than this threshold will be considered "
        "duplicates."
    ),
)
def group_duplicates(
    input_dir: Path,
    output_dir: Path,
    hashes_path: Path,
    threshold: int,
) -> None:
    """
    Union duplicate images into groups and copy them to output_dir.

    Note: you can use threshold up to 32 if non Keenetic are not important.

    \b
    Args:
        input_dir (Path):
            The directory containing the input images.
        output_dir (Path):
            The directory where grouped duplicate images will be copied.

    \b
    Example:
        poetry run group_duplicates -h data/new/hash.csv data/new/images/ dups/
    """
    # remove previous results from output_dir
    shutil.rmtree(output_dir, ignore_errors=True)
    os.makedirs(output_dir)

    dups = _group_duplicates(hashes_path, threshold)
    for group_name, dup_names in dups.items():
        if len(dup_names) == 1:
            continue  # skip non dups

        os.makedirs(os.path.join(output_dir, group_name))
        for name in dup_names:
            src_path = os.path.join(input_dir, name)
            trg_path = os.path.join(output_dir, group_name, name)
            shutil.copy(src_path, trg_path)


def _group_duplicates(
    hashes_path: Path,
    threshold: int,
) -> Dict[str, Set[str]]:
    """
    Union duplicate images into groups based on distances between p-hashes.

    Args:
        hashes_path (Path):
            The path to the CSV file containing p-hashes for images.
        threshold (int):
            The threshold for considering images as duplicates. Images with
            Hamming distance less than this threshold will be considered
            duplicates.

    Returns:
        Dict[str, Set[str]]:
            A dictionary where keys are representative images of each group
            (parent), and values are sets containing filenames of duplicate
            images belonging to each group.
    """
    # create unions of duplicates
    group = DisjointSet()
    for name1, name2, distance in calculate_distances(hashes_path):
        group.make_set(name1)
        group.make_set(name2)
        if distance < threshold:
            group.union(name1, name2)

    # find unions and save them to a dict
    dups = defaultdict(set)
    for image in group.parent:
        parent = group.find(image)
        dups[parent].add(image)

    return dups


class DisjointSet:
    """Class representing a Disjoint Set data structure."""

    def __init__(self):
        """Initialize the DisjointSet object."""
        self.parent = {}

    def make_set(self, x):
        """
        Create a new set with element x.

        Args:
            x: Element to create a new set with.
        """
        self.parent.setdefault(x, x)

    def find(self, x):
        """
        Find the representative (root) of the set containing element x.

        Args:
            x: Element to find its representative.

        Returns:
            The representative (root) of the set containing element x.
        """
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])

        return self.parent[x]

    def union(self, x, y):
        """
        Unites the sets containing elements x and y.

        Args:
            x: Element in the first set.
            y: Element in the second set.
        """
        parent_x = self.find(x)
        parent_y = self.find(y)
        if parent_x != parent_y:
            self.parent[parent_x] = parent_y


def calculate_distances(path: Path) -> Generator:
    """
    Calculate Hamming distances between p-hashes for all image pairs in a path.

    Args:
        path (Path): The path to the CSV file containing p-hashes of images.

    Yields:
        Tuple[str, str, int]: A tuple containing the names of two images and
                              their Hamming distance.

    Example:
        calculate_distances(Path("data/raw/phashes.csv"))
    """
    hashes = load_hashes(path)
    for (name1, hash1), (name2, hash2) in combinations(hashes, 2):
        hamming_distance = hash1 - hash2
        yield name1, name2, hamming_distance


@click.command()
@click.argument("images_dir", type=click.Path(exists=True))
@click.argument("hashes_path", type=click.Path(path_type=Path))
@click.option(
    "--hash-size",
    default=HASH_SIZE,
    show_default=True,
    type=click.IntRange(2, inf),
    help="Size of the hash.",
)
@click.option(
    "--image-formats",
    multiple=True,
    show_default=True,
    default=("jpg", "jpeg", "png", "gif"),
    help="Tuple of image formats to consider.",
)
def update_hashes(
    images_dir: Path,
    hashes_path: Path,
    hash_size: int,
    image_formats: Tuple[str],
) -> None:
    """
    Calculate p-hashes for all images in a path and save them to CSV file.

    \b
    Args:
        images_dir (Path):
            Path to the directory containing images.
        hashes_path (Path):
            Path to the CSV file where updated p-hashes will be saved.

    \b
    Example:
        poetry run update_hashes data/new/images/ data/new/hash.csv
    """
    hashes = calculate_hashes(images_dir, hash_size, image_formats)
    save_hashes(hashes, hashes_path)


def calculate_hashes(
    path: Path,
    hash_size: int,
    image_formats: Tuple[str],
) -> Generator:
    """
    Calculate p-hashes for all images in the specified directory.

    Args:
        path (Path): The path to the directory containing images.
        hash_size (int): The size of the hash to be generated.
        image_formats (Tuple[str]): A tuple containing allowed image formats.

    Yields:
        Tuple[str, ImageHash]: A tuple containing the filename and its
                                corresponding p-hash.
    """
    for filename in tqdm(os.listdir(path)):
        if filename.endswith(image_formats):
            image_path = os.path.join(path, filename)
            image = transform(Image.open(image_path))
            phash = imagehash.phash(image, hash_size=hash_size)

            yield filename, phash


def transform(
    image: Image.Image,
    resize: Tuple[int, int] = (1920, 1080),
    crop: Tuple[int, int, int, int] = CROP_BOX,
) -> Image.Image:
    """
    Apply transforms to optimize duplicate detection.

    Args:
        image (Image.Image):
            The input image.
        resize (Tuple[int, int]):
            The size to which the image will be resized.
        crop (Tuple[int, int, int, int]):
            The cropping box (left, upper, right, lower) to apply after
            resizing.

    Returns:
        Image.Image: The transformed image.

    Example:
        transform(image)
    """
    # Unique shapes: 1920 x 1080, 2688 x 1520, 704 x 576
    antialias = Image.Resampling.LANCZOS

    return image.convert("L").resize(resize, antialias).crop(crop)


def save_hashes(hashes: Iterable, output_path: Path) -> None:
    """
    Save hashes to CSV file.

    Args:
        hashes (Iterable): An iterable containing tuples of image names and
                           their corresponding p-hashes.
        output_path (Path): The path to the CSV file where hashes will be
                            saved.

    Example:
        save_hashes(hashes, Path("data/raw/phashes.csv"))
    """
    with open(output_path, mode="w", encoding="utf-8", newline="") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(["Name", "P-Hash"])
        csv_writer.writerows(hashes)


def load_hashes(path: Path) -> Generator:
    """
    Load hashes from CSV file.

    Args:
        path (Path): The path to the CSV file containing hashes.

    Yields:
        Tuple[str, ImageHash]: A tuple containing the name of an image and its
                                corresponding p-hash.

    Example:
        load_hashes(Path("data/raw/phashes.csv"))
    """
    with open(path, mode="r", encoding="utf-8") as csv_file:
        reader = csv.reader(csv_file)
        next(reader)  # skip the header row; pylint: disable=R1708
        for row in reader:
            name, hash_string = row
            yield name, imagehash.hex_to_hash(hash_string)
