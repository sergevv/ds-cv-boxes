# -*- coding: utf-8 -*-
"""
Module for creating classifier dataset from a segmentation dataset.

This module provides functions to extract router images from a COCO-formatted
segmentation dataset and resize them to fit specified dimensions. It also
includes a function to perform a stratified shuffle split on a dataset
directory, preserving the percentage of samples for each class.
"""

import os
import random
import shutil
from collections import Counter
from pathlib import Path
from typing import List

import click
import numpy as np
from PIL import Image
from tqdm import tqdm

from cv_boxes.data.auxiliary import copy_mask_from_image
from cv_boxes.data.paths import DETECTION_DATASET, TEMP_DIR
from cv_boxes.datastructures.dataset import CocoDataset

TARGET_WIDTH = 2688
TARGET_HEIGHT = 1520
TRAIN_SIZE = 0.8
CLASSIFIER_DATASET = os.path.join(TEMP_DIR, "classifier_dataset")
CLASSIFIER_TRAIN_SET = os.path.join(TEMP_DIR, "train")
CLASSIFIER_TEST_SET = os.path.join(TEMP_DIR, "test")


@click.command()
@click.option(
    "-i",
    "--input-dir",
    default=DETECTION_DATASET,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True),
    help="Path to the directory containing detection dataset in COCO format.",
)
@click.option(
    "-o",
    "--output-dir",
    default=CLASSIFIER_DATASET,
    show_default=True,
    type=click.Path(dir_okay=True),
    help="Path to the directory where extracted router images will be saved.",
)
@click.option(
    "--annotations",
    default="annotations.json",
    show_default=True,
    type=click.STRING,
    help="Name of the annotations file in JSON format.",
)
@click.option(
    "--images",
    default="images",
    show_default=True,
    type=click.STRING,
    help="Name of the directory containing images.",
)
def extract_routers(
    input_dir: Path, output_dir: Path, annotations: str, images: str
) -> None:
    """
    Extract router images from segmentation dataset.

    This function takes an input directory containing image data and
    annotations in COCO format and extracts router images from the segmentation
    dataset. It resizes the images to fit the specified dimensions and saves
    the extracted router images into a new directory structure corresponding to
    their classes.

    \b
    Example:
        poetry run extract_routers
    """
    images_path = os.path.join(input_dir, images)
    annotations_path = os.path.join(input_dir, annotations)
    detection_dataset = CocoDataset.from_file(annotations_path)
    count: Counter = Counter()

    shutil.rmtree(output_dir, ignore_errors=True)
    for label in detection_dataset.labels:
        class_path = os.path.join(output_dir, label)
        os.makedirs(class_path)

    for sample in tqdm(detection_dataset):
        image_path = os.path.join(images_path, sample.name)
        pil_image = Image.open(image_path)

        width, height = pil_image.size
        scale_ratio = min(TARGET_WIDTH / width, TARGET_HEIGHT / height)
        new_width = int(width * scale_ratio)
        new_height = int(height * scale_ratio)
        pil_image = pil_image.resize((new_width, new_height), Image.LANCZOS)

        image = np.array(pil_image)
        for annotation in sample.annotations:
            points = np.array(annotation.mask, dtype=np.float32)
            points = points.reshape(-1, 2)
            points *= scale_ratio

            extracted_box = copy_mask_from_image(points, image)

            class_name = detection_dataset.get_class_label(annotation.label)
            count[class_name] += 1
            image_name = f"{count[class_name]:04d}.jpg"
            output_path = os.path.join(output_dir, class_name, image_name)
            Image.fromarray(extracted_box).save(output_path)


@click.command()
@click.option(
    "--dataset-dir",
    default=CLASSIFIER_DATASET,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True),
    help="Directory containing the full dataset.",
)
@click.option(
    "--train-dir",
    default=CLASSIFIER_TRAIN_SET,
    show_default=True,
    type=click.Path(dir_okay=True),
    help="Directory where the training set will be saved.",
)
@click.option(
    "--test-dir",
    default=CLASSIFIER_TEST_SET,
    show_default=True,
    type=click.Path(dir_okay=True),
    help="Directory where the testing set will be saved.",
)
@click.option(
    "--train_size",
    default=TRAIN_SIZE,
    show_default=True,
    type=click.FloatRange(min=0, max=1, min_open=True, max_open=True),
    help="Proportion of the dataset to include in the training set.",
)
def stratified_shuffle_split(
    dataset_dir: Path,
    train_dir: Path,
    test_dir: Path,
    train_size: float,
) -> None:
    """
    Perform stratified shuffle split on a classifier dataset.

    The split is made by preserving the percentage of samples for each class.

    \b
    Example:
        poetry run stratified_shuffle_split
    """
    shutil.rmtree(train_dir, ignore_errors=True)
    shutil.rmtree(test_dir, ignore_errors=True)

    for class_name in os.listdir(dataset_dir):
        class_path = os.path.join(dataset_dir, class_name)
        labels = os.listdir(class_path)
        random.shuffle(labels)
        size = len(labels)
        boundary = int(size * train_size)

        train_labels = labels[:boundary]
        train_path = os.path.join(train_dir, class_name)
        _copy_files(class_path, train_path, train_labels)

        test_labels = labels[boundary:]
        test_path = os.path.join(test_dir, class_name)
        _copy_files(class_path, test_path, test_labels)


def _copy_files(
    source_dir: str,
    destination_dir: str,
    file_list: List[str],
) -> None:
    """
    Copy a list of files from a source directory to a destination directory.

    Args:
        source_dir (str):
            The path to the source directory containing the files to be copied.
        destination_dir (str):
            The path to the destination directory where the files will be
            copied.
        file_list (List[str]):
            A list of filenames to be copied from the source directory.
    """
    os.makedirs(destination_dir, exist_ok=True)
    for filename in file_list:
        source_path = os.path.join(source_dir, filename)
        destination_path = os.path.join(destination_dir, filename)
        shutil.copy(source_path, destination_path)
