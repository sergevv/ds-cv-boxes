# -*- coding: utf-8 -*-
"""Module for checking the validity of annotations in a COCO dataset."""
from bisect import bisect_left
from datetime import datetime
from itertools import combinations
from math import sqrt
from pathlib import Path
from typing import List, Optional, Tuple

import click
from shapely.geometry import Polygon

from cv_boxes.data.bounds import BoundingBox, router_bounds
from cv_boxes.data.paths import DETECTION_ANNOTATIONS
from cv_boxes.datastructures.dataset import CocoDataset
from cv_boxes.datastructures.predictions import Predictions
from cv_boxes.models.constants import DATE_FORMAT

DATE_LEN = 14
IOU_THRESHOLD = 0.1
X_MIN = 500
X_MAX = 1300
Y_MIN = 400
Y_MAX = 640
AREA_MIN = 1300
AREA_MAX = 7000
SEGMENTATION_LEN = 8

TOTAL = 0
BOX_LIMITS = (
    {"WIDTH": {"MIN": 10, "MAX": 45}, "HEIGHT": {"MIN": 70, "MAX": 220}},
    {"WIDTH": {"MIN": 19, "MAX": 36}, "HEIGHT": {"MIN": 119, "MAX": 161}},
    {"WIDTH": {"MIN": 16, "MAX": 34}, "HEIGHT": {"MIN": 106, "MAX": 208}},
    {"WIDTH": {"MIN": 13, "MAX": 37}, "HEIGHT": {"MIN": 125, "MAX": 197}},
    {"WIDTH": {"MIN": 15, "MAX": 41}, "HEIGHT": {"MIN": 74, "MAX": 107}},
    {"WIDTH": {"MIN": 19, "MAX": 36}, "HEIGHT": {"MIN": 119, "MAX": 160}},
)

TARGET_WIDTH = 1920
TARGET_HEIGHT = 1080


@click.command()
@click.option(
    "--annotations-path",
    default=DETECTION_ANNOTATIONS,
    show_default=True,
    type=click.Path(exists=True),
    help="Path to the annotations JSON file in COCO format.",
)
def check_annotations(annotations_path: Path):
    """
    Check that annotation values are within acceptable ranges.

    \b
    This function checks various aspects of annotations in a COCO dataset:
        1. The length of segmentations for each annotation.
        2. The areas of segmentations for each annotation.
        3. The sizes (width and height) of bounding boxes for each annotation.
        4. The sizes of bounding boxes categorized by object category.
        5. The positions (coordinates) of bounding boxes for each annotation.
        6. The intersections between bounding boxes.

    If all tests pass without errors, it prints a message indicating that all
    annotations seem to be correct.

    \b
    Example:
        poetry run check_annotations
    """
    dataset = CocoDataset.from_file(annotations_path)

    check_segmentation_len(dataset=dataset)
    check_segmentation_areas(dataset=dataset)
    check_sizes(dataset=dataset)
    check_sizes_by_category(dataset=dataset)
    check_bbox_positions(dataset=dataset)
    check_intersections(dataset=dataset)

    click.echo("All tests passed. Annotations seems to be correct.")


def check_segmentation_len(dataset) -> None:
    """
    Check that segmentation lengths are within a sanity range.

    This function iterates through each image in the dataset and checks the
    length of segmentation masks for each annotation. If the length is not
    equal to SEGMENTATION_LEN, it raises an AssertionError with a descriptive
    message indicating the issue.

    Args:
        dataset: The dataset containing images and annotations.

    Raises:
        ValueError: If the length of segmentation mask is invalid for any
                    annotation.
    """
    for image in dataset:
        for annotation in image.annotations:
            label = dataset.categories[annotation.label]

            if len(annotation.mask) != SEGMENTATION_LEN:
                raise ValueError(
                    "Invalid length of semgentation mask for image "
                    f"{image.name} ({annotation.mask}, label: {label}."
                )


def check_sizes(dataset) -> None:
    """
    Check that segmentation-oriented envelope sizes are within a sanity range.

    Args:
        dataset (List[Image]): A list of Image objects containing annotations.

    Raises:
        ValueError: If the calculated box size (width or height) is outside of
                    the normal values for any annotation within the dataset.
    """
    for image in dataset:
        for annotation in image.annotations:
            xs = annotation.mask[0::2]  # noqa: WPS349
            ys = annotation.mask[1::2]
            xs, ys = correct_pos_to_target_size(
                image.width, image.height, xs, ys
            )
            poly = Polygon(zip(xs, ys))
            coords = list(poly.oriented_envelope.exterior.coords)
            edge_lengths = [
                sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
                for (x1, y1), (x2, y2) in zip(coords, coords[1:])
            ]
            box_width = min(edge_lengths)
            box_height = max(edge_lengths)

            min_width = BOX_LIMITS[TOTAL]["WIDTH"]["MIN"]
            max_width = BOX_LIMITS[TOTAL]["WIDTH"]["MAX"]
            min_height = BOX_LIMITS[TOTAL]["HEIGHT"]["MIN"]
            max_height = BOX_LIMITS[TOTAL]["HEIGHT"]["MAX"]

            label = dataset.categories[annotation.label]

            width_is_within_range = min_width <= box_width <= max_width
            height_is_within_range = min_height <= box_height <= max_height
            if not width_is_within_range or not height_is_within_range:
                raise ValueError(
                    "Box size outside of normal values for image "
                    f"{image.name}, label `{label}` "
                    f"({box_width} x {box_height})."
                )


def correct_pos_to_target_size(width, height, xs, ys):
    """
    Correct positions relative to a target size.

    Args:
        width (int): The original width of the source.
        height (int): The original height of the source.
        xs (List[int]): List of x-coordinates to be corrected.
        ys (List[int]): List of y-coordinates to be corrected.

    Returns:
        Tuple[List[int], List[int]]:
            A tuple containing two lists:
                1. List of corrected x-coordinates.
                2. List of corrected y-coordinates.
    """
    xs = [int(x * TARGET_WIDTH / width) for x in xs]
    ys = [int(y * TARGET_HEIGHT / height) for y in ys]

    return xs, ys


def check_sizes_by_category(dataset) -> None:
    """
    Check segmentation sizes are within sanity range for their category.

    This function iterates over each image in the dataset and checks the size
    of segmentation-oriented envelope boxes for each annotation within the
    image. It compares the calculated box size (width and height) to predefined
    minimum and maximum limits for the corresponding annotation category.
    If the box size exceeds these limits, an AssertionError is raised.

    Args:
        dataset (List[Image]):
            A list of Image objects representing the dataset.
            Each Image object contains annotations.

    Raises:
        ValueError: If calculated box size (width or height) is outside
                    specified normal range for any annotation in dataset.
    """
    for image in dataset:
        for annotation in image.annotations:
            xs = annotation.mask[0::2]  # noqa: WPS349
            ys = annotation.mask[1::2]
            xs, ys = correct_pos_to_target_size(
                image.width, image.height, xs, ys
            )
            poly = Polygon(zip(xs, ys))
            coords = list(poly.oriented_envelope.exterior.coords)
            edge_lengths = [
                sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
                for (x1, y1), (x2, y2) in zip(coords, coords[1:])
            ]
            box_width = min(edge_lengths)
            box_height = max(edge_lengths)

            min_width = BOX_LIMITS[annotation.label]["WIDTH"]["MIN"]
            max_width = BOX_LIMITS[annotation.label]["WIDTH"]["MAX"]
            min_height = BOX_LIMITS[annotation.label]["HEIGHT"]["MIN"]
            max_height = BOX_LIMITS[annotation.label]["HEIGHT"]["MAX"]

            label = dataset.categories[annotation.label]

            width_is_within_range = min_width <= box_width <= max_width
            height_is_within_range = min_height <= box_height <= max_height
            if not width_is_within_range or not height_is_within_range:
                raise ValueError(
                    "Box size outside of normal values for image "
                    f"{image.name}, label `{label}` "
                    f"({box_width} x {box_height})."
                )


def check_segmentation_areas(dataset) -> None:
    """
    Check that segmentation areas are within the sanity range.

    Args:
        dataset (List[Image]):
            A list of Image objects representing the dataset.
            Each Image object contains annotations and image metadata.

    Raises:
        ValueError: If the area of any bounding polygon is outside the normal
                    values for the corresponding image.
    """
    for image in dataset:
        for annotation in image.annotations:
            xs = annotation.mask[0::2]  # noqa: WPS349
            ys = annotation.mask[1::2]
            xs, ys = correct_pos_to_target_size(
                image.width, image.height, xs, ys
            )
            area = Polygon(zip(xs, ys)).area

            label = dataset.categories[annotation.label]

            if area < AREA_MIN or area > AREA_MAX:
                raise ValueError(
                    "Bounding polygon area outside of normal values for image "
                    f"{image.name}, `{label}`, {area}, x = {xs}, y = {ys}."
                )


def check_bbox_positions(dataset) -> None:
    """
    Check that bounding box positions are within a sane range.

    Args:
        dataset (List[Image]): A list of Image objects containing annotations.

    Raises:
        ValueError: If bounding box coordinates are outside of the stock area
                    for an image.
    """
    for image in dataset:
        for annotation in image.annotations:
            x = min(annotation.mask[0::2])  # noqa: WPS349
            y = min(annotation.mask[1::2])
            label = dataset.categories[annotation.label]

            if not is_pos_within_bounds(x, y, image.name[:DATE_LEN]):
                raise ValueError(
                    "Bounding box coordinates outside of stock area for image "
                    f"{image.name} label, `{label}` ({x}, {y})."
                )


def remove_invalid_predictions(
    predictions: Predictions,
    date: Optional[str],
    image_size: Tuple[int, int],
) -> Predictions:
    """
    Remove invalid predictions from input based on specified criteria.

    The function filters out predictions that do not meet the following
    criteria:
        1. The position of the prediction's bounding box is within the bounds
           for the given date.
        2. The area of the prediction's polygon mask is within the specified
           minimum and maximum area range.

    Finally, non-maximum suppression (NMS) is applied to remove overlapping
    predictions, and the filtered predictions are returned.

    Args:
        predictions (Predictions): The predictions to filter.
        date (Optional[str]): The date of the image.
        image_size (Tuple[int, int]): Image size in pixels (width, height).

    Returns:
        Predictions: Filtered set of predictions.
    """
    if date is None:
        date = datetime.now().strftime(DATE_FORMAT)

    valid_predictions = []
    image_width, image_height = image_size
    scale_factor = min(
        TARGET_WIDTH / image_width,
        TARGET_HEIGHT / image_height,
    )
    for prediction in predictions:
        left, top = prediction.bbox[:2]
        if not is_pos_within_bounds(left, top, date):
            continue

        poly = Polygon(prediction.polygon_mask)
        area = poly.area * scale_factor * scale_factor
        if area < AREA_MIN or area > AREA_MAX:
            continue

        valid_predictions.append(prediction)

    ret = Predictions.from_list(
        predictions=valid_predictions,
        classes=predictions.classes,
        palette=predictions.palette,
    )

    return ret.nms()


def is_pos_within_bounds(x: float, y: float, date: str) -> bool:
    """
    Check if router position is within bounds for camera pos at a given date.

    Args:
        x (float): The x-coordinate of router position.
        y (float): The y-coordinate of router position.
        date (str): The date for which camera position bounds are considered.

    Returns:
        bool: True if the router position is within bounds, False otherwise.
    """
    x_min, y_min, x_max, y_max = get_bounds(date)

    return x_min <= x <= x_max and y_min <= y <= y_max


def get_bounds(date: str) -> BoundingBox:
    """
    Get router position bounds for a given date.

    Args:
        date (str): The date for which to retrieve the router position bounds.

    Returns:
        BoundingBox: bounding box of router positions for the specified date.
    """
    dates = list(router_bounds)
    i = bisect_left(dates, date) - 1
    bounds_date = dates[i]

    return router_bounds[bounds_date]


def check_intersections(dataset) -> None:
    """
    Check intersections of segmentation areas within sanity range.

    Args:
        dataset (List[Image]): A list of Image objects containing annotations.

    Raises:
        ValueError: If the intersection over union (IOU) of segmentation areas
                    is below the threshold.
    """
    for image in dataset:
        for annotation1, annotation2 in combinations(image.annotations, 2):
            poly1 = _seg_to_poly(annotation1.mask)
            poly2 = _seg_to_poly(annotation2.mask)

            intersection_area = poly1.intersection(poly2).area
            union_area = poly1.union(poly2).area
            iou = intersection_area / union_area if union_area > 0 else 0

            if iou > IOU_THRESHOLD:
                m = f"Duplicate bounding box for image {image.name} at {poly1}"
                raise ValueError(m)


def _seg_to_poly(seg_mask: List[int]) -> Polygon:
    """
    Convert a segmentation mask from a list of integers to a Shapely Polygon.

    Args:
        seg_mask (List[int]):
            A list of integers representing a segmentation mask. It contains
            alternating x and y coordinates of the polygon vertices.

    Returns:
        Polygon:
            A Shapely Polygon object representing the polygon defined by the
            segmentation mask.
    """
    xs = seg_mask[0::2]  # noqa: WPS349
    ys = seg_mask[1::2]

    return Polygon(zip(xs, ys))
