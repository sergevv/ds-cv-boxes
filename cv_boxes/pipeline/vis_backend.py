# -*- coding: utf-8 -*-
"""
Module providing a custom MLflow visualization backend.

This module contains a custom implementation of the MLflow visualization
backend, addressing a specific bug.
"""

from mmengine.config import Config
from mmengine.registry import VISBACKENDS
from mmengine.visualization import MLflowVisBackend
from mmengine.visualization.vis_backend import force_init_env

STRING_LIMIT = 500


@VISBACKENDS.register_module()
class MLflowVisBackendCustom(MLflowVisBackend):
    """Fix bug in MLflowVisBackend."""

    @force_init_env
    def add_config(self, config: Config, **kwargs) -> None:
        """Log configuration to the MLflow visualization backend.

        This method logs configuration to the MLflow visualization backend.

        Args:
            config (Config): Configuration object.
            kwargs: Additional keyword arguments.

        """
        self.cfg = config  # pylint: disable=W0201
        if self._tracked_config_keys is None:
            # crop param length to mlflow limit
            cfg = self._flatten(self.cfg)
            for k, v in cfg.items():
                if len(str(v)) > STRING_LIMIT:
                    cfg[k] = str(v)[:STRING_LIMIT]
            self._mlflow.log_params(cfg)
        else:
            tracked_cfg = {}
            for key in self._tracked_config_keys:
                tracked_cfg[key] = self.cfg[key]
            self._mlflow.log_params(self._flatten(tracked_cfg))
        self._mlflow.log_text(self.cfg.pretty_text, "config.py")
