# -*- coding: utf-8 -*-
"""
Module for custom image transformations.

Examples:
    >>> from mmengine.transforms import Crop, RandomRotate90
    >>> crop = Crop(left=10, top=20, right=100, bottom=200)
    >>> rotated = RandomRotate90(p=0.75)
    >>> transformed_image = crop.transform(image)
    >>> transformed_data = rotated.transform(data)
"""
from typing import Dict, List, Tuple

import numpy as np
from mmcv import imrotate
from mmcv.transforms import BaseTransform
from mmcv.transforms.processing import CenterCrop
from mmengine.registry import TRANSFORMS


@TRANSFORMS.register_module()
class Crop(CenterCrop):
    """
    Crop the image, segmentation masks, and bounding boxes.

    This class implements cropping functionality for images, segmentation
    masks, bounding boxes, and keypoints. It inherits from CenterCrop, which
    crops the input image based on a central region defined by the given
    parameters.

    Args:
        left (int): The x-coordinate of the left boundary of the crop.
        top (int): The y-coordinate of the top boundary of the crop.
        right (int): The x-coordinate of the right boundary of the crop.
        bottom (int): The y-coordinate of the bottom boundary of the crop.
    """

    def __init__(self, left: int, top: int, right: int, bottom: int) -> None:
        """
        Initialize Crop with crop coordinates.

        Args:
            left (int): The x-coordinate of the left boundary of the crop.
            top (int): The y-coordinate of the top boundary of the crop.
            right (int): The x-coordinate of the right boundary of the crop.
            bottom (int): The y-coordinate of the bottom boundary of the crop.
        """
        super().__init__((right - left, bottom - top))
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom

    def transform(self, results: dict) -> dict:
        """Apply crop on results containing image data.

        Args:
            results (dict): Result dict containing the data to transform.

        Returns:
            dict: Results with cropped image and semantic segmentation map.

        Raises:
            ValueError: If 'img' is not found in results.
        """
        if "img" not in results:
            raise ValueError("`img` is not found in results")

        bboxes = np.array(
            [self.left, self.top, self.right - 1, self.bottom - 1],
        )
        self._crop_img(results, bboxes)
        self._crop_seg_map(results, bboxes)
        self._crop_bboxes(results, bboxes)
        self._crop_masks(results)
        self._crop_keypoints(results, bboxes)
        self._record_homography_matrix(results)

        return results

    def __repr__(self) -> str:
        """
        Return a string representation of the Crop object.

        Returns:
            str: String representation of the Crop object.
        """
        repr_str = [
            self.__class__.__name__,
            f"(left = {self.left}",
            f", (top = {self.top}",
            f", (right = {self.right}",
            f", (bottom = {self.bottom}",
        ]
        return "".join(repr_str)

    def _crop_bboxes(self, results: Dict, bboxes: np.ndarray) -> None:
        """
        Crop bounding boxes according to the cropping window.

        Args:
            results (Dict): data to transform.
            bboxes (np.ndarray): Array of bounding boxes.
        """
        if results.get("gt_bboxes") is not None:
            results["gt_bboxes"].translate_(-bboxes[:2])

    def _crop_masks(self, results: Dict) -> None:
        """
        Crop segmentation masks based on the crop coordinates.

        Args:
            results (Dict): data to transform.
        """
        if results.get("gt_masks") is not None:
            bbox = np.array([self.left, self.top, self.right, self.bottom])
            results["gt_masks"] = results["gt_masks"].crop(bbox)

    def _record_homography_matrix(self, results: Dict) -> None:
        """
        Record the homography matrix for the crop transformation.

        Args:
            results (Dict): data to transform.
        """
        homography_matrix = np.array(
            [
                [1, 0, -self.left],
                [0, 1, -self.top],
                [0, 0, 1],
            ],
            dtype=np.float64,
        )
        if results.get("homography_matrix") is not None:
            homography_matrix @= results["homography_matrix"]

        results["homography_matrix"] = homography_matrix


@TRANSFORMS.register_module()
class RandomRotate(BaseTransform):
    """
    Apply random rotation transformation to images, bounding boxes, and masks.

    Args:
        max_rotate_degree (float): Maximum degree of rotation. Defaults to 180.
        p (float): Probability of applying the transformation. Defaults to 0.5.

    Examples:
        >>> random_rotate = RandomRotate(max_rotate_degree=90, p=0.8)
        >>> transformed_data = random_rotate.transform(data)
    """

    def __init__(self, max_rotate_degree: float = 180, p: float = 0.5) -> None:
        """
        Initialize RandomRotate transformation with given parameters.

        Args:
            max_rotate_degree (float): Maximum rotation angle. Defaults to 180.
            p (float): Probability of applying transformation. Defaults to 0.5.
        """
        super().__init__()
        self.max_rotate_degree = max_rotate_degree
        self.p = p

    def transform(self, results: Dict) -> Dict | Tuple[List] | None:
        """
        Apply random rotation to input image and related annotations.

        Args:
            results (Dict):
                Input data containing image, bounding boxes, masks, etc.

        Returns:
            Dict | Tuple[List] | None:
                Transformed results including image, bounding boxes, masks,
                and homography matrix.
        """
        if np.random.rand() < self.p:
            angle = np.random.uniform(
                low=-self.max_rotate_degree,
                high=self.max_rotate_degree,
            )
            self._rotate_img(results, angle)
            self._rotate_bboxes(results, angle)
            self._rotate_masks(results, angle)
            self._record_homography_matrix(results, angle)

        return results

    def __repr__(self) -> str:
        """
        Return a string representation of RandomRotate.

        Returns:
            str: String representation of the object.
        """
        repr_str = [
            self.__class__.__name__,
            f"(max_rotate_degree = {self.max_rotate_degree}",
            f"(, p = {self.p}",
        ]
        return "".join(repr_str)

    @classmethod
    def homography_matrix(
        cls,
        angle: float,
        cx: float,
        cy: float,
    ) -> np.ndarray:
        """
        Create a 2D homography matrix for rotation around a specified center.

        Args:
            angle (float): Rotation angle in degrees.
            cx (float): X coordinate of the center.
            cy (float): Y coordinate of the center.

        Returns:
            np.ndarray: Homography matrix for rotation.
        """
        # Translate center of the image to the origin
        t1 = cls.translation_matrix(-cx, -cy)
        # Rotate the image around the origin
        r = cls.rotation_matrix(angle)
        # Translate the origin back to the center of the image
        t2 = cls.translation_matrix(cx, cy)
        # Combine the transformations
        return t2.dot(r).dot(t1)

    @classmethod
    def rotation_matrix(cls, angle: float) -> np.ndarray:
        """
        Compute the rotation matrix.

        Args:
            angle (float): Rotation angle in degrees.

        Returns:
            np.ndarray: Rotation matrix.
        """
        angle_rad = np.deg2rad(angle)
        cos_theta = np.cos(angle_rad)
        sin_theta = np.sin(angle_rad)
        return np.array(
            [
                [cos_theta, -sin_theta, 0],
                [sin_theta, cos_theta, 0],
                [0, 0, 1],
            ],
            dtype=np.float64,
        )

    @classmethod
    def translation_matrix(cls, tx: float, ty: float) -> np.ndarray:
        """
        Compute the translation matrix.

        Args:
            tx (float): Translation in the x-direction.
            ty (float): Translation in the y-direction.

        Returns:
            np.ndarray: Translation matrix.
        """
        return np.array(
            [
                [1, 0, tx],
                [0, 1, ty],
                [0, 0, 1],
            ],
            dtype=np.float64,
        )

    def _rotate_img(self, results: Dict, angle: float) -> None:
        """
        Rotate the input image.

        Args:
            results (Dict): Input image and annotations.
            angle (float): Rotation angle in degrees.
        """
        image = results["img"]
        results["img"] = imrotate(image, angle)

    def _rotate_bboxes(self, results: Dict, angle: float) -> None:
        """
        Rotate bounding boxes.

        Args:
            results (Dict): Input image and annotations.
            angle (float): Rotation angle in degrees.
        """
        image = results["img"]
        center = np.array(image.shape[:2]) / 2
        results["gt_bboxes"].rotate_(center, angle)

    def _rotate_masks(self, results: Dict, angle: float) -> None:
        """
        Rotate segmentation masks.

        Args:
            results (Dict): Input image and annotations.
            angle (float): Rotation angle in degrees.
        """
        if results.get("gt_masks") is not None:
            shape = results["img"].shape[:2]
            center = np.array(shape) / 2
            results["gt_masks"] = results["gt_masks"].rotate(
                shape,
                angle,
                center,
            )

    def _record_homography_matrix(self, results: Dict, angle: float) -> None:
        """
        Record the homography matrix for the rotation.

        Args:
            results (Dict): Input image and annotations.
            angle (float): Rotation angle in degrees.
        """
        shape = results["img"].shape[:2]
        cx, cy = np.array(shape) / 2
        homography_matrix = self.homography_matrix(angle, cx, cy)
        if results.get("homography_matrix") is not None:
            homography_matrix @= results["homography_matrix"]

        results["homography_matrix"] = homography_matrix


@TRANSFORMS.register_module()
class RandomRotate90(RandomRotate):
    """
    Apply random rotations to images, bboxes, and masks.

    Apply random rotations of 90, 180, or 270 degrees to images, bounding
    boxes, and masks with a given probability.

    Args:
        p (float): Probability of applying the rotation. Defaults to 0.75.

    Returns:
        Dict | Tuple[List] | None:
            Transformed results containing rotated images, bounding boxes,
            masks, and homography matrix. Returns None if no transformation is
            applied.

    Examples:
        >>> transform = RandomRotate90(p=0.75)
        >>> results = {
        >>>     'image': np.zeros((256, 256, 3), dtype=np.uint8),
        >>>     'bboxes': [[10, 10, 100, 100]],
        >>>     'masks': np.zeros((256, 256), dtype=np.uint8)
        >>> }
        >>> transformed_results = transform.transform(results)
    """

    def __init__(self, p: float = 0.75) -> None:
        """
        Initialize RandomRotate90 with the given probability.

        Args:
            p (float): Probability of applying the rotation. Defaults to 0.75.
        """
        super().__init__(p=p)

    def transform(self, results: Dict) -> Dict | Tuple[List] | None:
        """
        Apply random rotation to images, bounding boxes and masks.

        Args:
            results (Dict):
                A dictionary containing image, bounding boxes, and masks.

        Returns:
            Dict | Tuple[List] | None:
                Transformed results containing rotated images, bounding boxes,
                masks, and homography matrix.
        """
        if np.random.rand() < self.p:
            angle = np.random.choice((90, 180, 270))
            self._rotate_img(results, angle)
            self._rotate_bboxes(results, angle)
            self._rotate_masks(results, angle)
            self._record_homography_matrix(results, angle)

        return results

    def __repr__(self) -> str:
        """
        Return a string representation of the RandomRotate90 object.

        Returns:
            str: String representation of the RandomRotate90 object.
        """
        repr_str = [
            self.__class__.__name__,
            f"(p = {self.p}",
        ]
        return "".join(repr_str)
