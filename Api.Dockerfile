FROM python:3.11

RUN apt-get update && apt-get install -y libgl1

RUN addgroup -gid 500 api && useradd -m -u 500 -g 500 -s /bin/bash api

ARG ROOT=/home/api/app
WORKDIR $ROOT

RUN git clone https://gitlab.com/sergevv/ds-cv-boxes .

RUN mkdir -p $ROOT/models
COPY models/chosen/ $ROOT/models/chosen/
COPY lib/ $ROOT/lib/
RUN cp .env_example .env

RUN chown -R api:api $ROOT

USER api

RUN curl -sSL https://install.python-poetry.org | python3 -

ENV PATH="/home/api/.local/bin:$PATH"

RUN poetry install --no-interaction --no-ansi --only api

cmd [ "make", "start-api" ]
