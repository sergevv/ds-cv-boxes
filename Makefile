#################################################################################
# GLOBALS                                                                       #
#################################################################################

include .env
export

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = cv-boxes
PYTHON_INTERPRETER = python3

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## start API
start-api:
	poetry run start_api

## start telegram bot
start-bot:
	poetry run start_bot

## Split dataset into train & test subsets
train-test-split:
	poetry run train_test_split

## Convert model from MMDetection to ONNX format
onnx:
	poetry run python3 \
		work_dirs/mmdeploy/tools/deploy.py \
		work_dirs/mmdeploy/configs/mmdet/instance-seg/instance-seg_onnxruntime_dynamic.py \
		models/chosen.pth/config.py \
		models/chosen.pth/model.pth \
		data/raw/images/20240604174944.jpg \
		--work-dir models/chosen \
		--device cpu \
		--dump-info

## start api-server and telegram bot
start-server: create-dirs
	docker compose up -d

## create dirs for logs
create-dirs:
	sudo mkdir -p /data/logs/cv-boxes-api
	sudo mkdir -p /data/logs/cv-boxes-bot
	sudo chown 500:500 /data/logs/cv-boxes-*/

## stop api-server and telegram bot
stop-server:
	-docker compose down

## Build API docker image
api-image:
	docker buildx build \
		--no-cache \
		--push \
		--platform linux/amd64,linux/arm64 \
		-f Api.Dockerfile \
		-t wervlad/cv-boxes-api:latest \
		-t wervlad/cv-boxes-api:$(VERSION) .

## Build telegram bot docker image
bot-image:
	docker buildx build \
		--no-cache \
		--push \
		--platform linux/amd64,linux/arm64 \
		-f Bot.Dockerfile \
		-t wervlad/cv-boxes-bot:latest \
		-t wervlad/cv-boxes-bot:$(VERSION) .

work_dirs/mmdeploy:
	cd work_dirs && git clone https://github.com/open-mmlab/mmdeploy.git

visualize-dataset:
	poetry run python3 \
		.venv/lib/python3.11/site-packages/mmdet/.mim/tools/analysis_tools/browse_dataset.py \
		checkpoints/mask-rcnn_r50-caffe_fpn_ms-poly-3x_router.py \
		--output-dir work_dirs/processed_dataset \
		--show-interval .01

## Run tests and linters
tests: black pylint mypy flake8 model-tests

## Check types
mypy:
	-poetry run mypy $$(git ls-files "*.py")

## Lint using flake8
flake8:
	-poetry run flake8 $$(git ls-files "*.py")

## Format code with black
black:
	-poetry run black --line-length 79 .

## Lint using pylint
pylint:
	-poetry run pylint $$(git ls-files "*.py")

## Check notebooks with linters
check-notebooks:
	-poetry run nbqa mypy $$(git ls-files "*.ipynb")
	-poetry run nbqa flake8 $$(git ls-files "*.ipynb")

## Strip out notebooks output
strip-notebooks:
	-poetry run nbstripout $$(git ls-files "*.ipynb")

## Run model tests
model-tests:
	echo "test model"

# this is only required for Linux versions of Hikvision SDK
# use this command if you experience
# `NET_DVR_RealPlay_V40 failed, error code: 64`
## fix rpath for Hikvision shared libraries
fix-hikvision-path:
	patchelf --set-rpath ./lib lib/libPlayCtrl.so

## Generate tags for project files
tags:
	ctags -f tags -R --fields=+iaS --extra=+q $(SOURCES)

## Generate tags for project files and libraries
include-tags:
	ctags -f include_tags -R --languages=python --fields=+iaS --extra=+q \
		.venv/lib/python*/

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete
	-rm tags include_tags

.PHONY: clean help onnx black pylint mypy flake8 model-tests

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
